module.exports = function(grunt) {
  
  grunt.initConfig({
    bower_concat: {
      all: {
        dest: 'app/assets/scripts/build/app.js',
        cssDest: 'app/assets/stylesheets/build/app.css',
        includeDev: true,
        mainFiles: {
          jquery: 'dist/jquery.min.js',
          moment: 'min/moment-with-locales.min.js',
          bootstrap: ['dist/css/bootstrap.min.css', 'dist/js/bootstrap.min.js'],
          'startbootstrap-sb-admin-2': [
            '../metisMenu/dist/metisMenu.min.css',
            '../morrisjs/morris.css',
            'dist/css/timeline.css',
            'dist/css/sb-admin-2.css',
            '../metisMenu/dist/metisMenu.min.js',
            '../raphael/raphael-min.js',
            '../morrisjs/morris.min.js',
            'dist/js/sb-admin-2.js'
            ],
          chosen: [
            'chosen.jquery.min.js',
            'chosen.min.css'
            ],
          alertify: [
            'alertify.min.js',
            'themes/alertify.core.css',
            'themes/alertify.default.css',
            'themes/alertify.bootstrap.css'
            ]
        },
        exclude: ['datatables-plugins', 'font-awesome']
      }
    }
  });

  grunt.loadNpmTasks('grunt-bower-concat');

  grunt.registerTask('default', ['bower_concat']);
};
