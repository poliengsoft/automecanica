var adminStaffsCtrl = rootRequire('app/controllers/admin/staffsController');
var adminTeamsCtrl = rootRequire('app/controllers/admin/teamsController');
var adminAutoPartsCtrl = rootRequire('app/controllers/admin/autoPartsController');
var adminServicesCtrl = rootRequire('app/controllers/admin/servicesController');
var adminClientsCtrl = rootRequire('app/controllers/admin/clientsController');
var adminVehiclesCtrl = rootRequire('app/controllers/admin/vehiclesController');
var adminDashboardCtrl = rootRequire('app/controllers/admin/dashboardController');
var adminAdminCtrl = rootRequire('app/controllers/admin/adminController');
var adminPasswordCtrl = rootRequire('app/controllers/admin/passwordController');
var adminReportsCtrl = rootRequire('app/controllers/admin/reportsController');
var adminSchedulesCtrl = rootRequire('app/controllers/admin/schedulesController');
var adminSosCtrl = rootRequire('app/controllers/admin/sosController');
var adminRevisionsCtrl = rootRequire('app/controllers/admin/revisionsController');

var attendantClientsCtrl = rootRequire('app/controllers/attendant/clientsController');
var attendantVehiclesCtrl = rootRequire('app/controllers/attendant/vehiclesController');
var attendantSchedulesCtrl = rootRequire('app/controllers/attendant/schedulesController');
var attendantDashboardCtrl = rootRequire('app/controllers/attendant/dashboardController');
var attendantAttendantCtrl = rootRequire('app/controllers/attendant/attendantController');
var attendantPasswordCtrl = rootRequire('app/controllers/attendant/passwordController');
var attendantSosCtrl = rootRequire('app/controllers/attendant/sosController');

var technicianTeamsCtrl = rootRequire('app/controllers/technician/teamsController');
var technicianSosCtrl = rootRequire('app/controllers/technician/sosController');
var technicianDashboardCtrl = rootRequire('app/controllers/technician/dashboardController');
var technicianClientsCtrl = rootRequire('app/controllers/technician/clientsController');
var technicianAutoPartsCtrl = rootRequire('app/controllers/technician/autoPartsController');
var technicianServicesCtrl = rootRequire('app/controllers/technician/servicesController');
var technicianSchedulesCtrl = rootRequire('app/controllers/technician/schedulesController');
var technicianRevisionsCtrl = rootRequire('app/controllers/technician/revisionsController');
var technicianTechnicianCtrl = rootRequire('app/controllers/technician/technicianController');
var technicianPasswordCtrl = rootRequire('app/controllers/technician/passwordController');

var clientClientCtrl = rootRequire('app/controllers/client/clientController');
var clientPasswordCtrl = rootRequire('app/controllers/client/passwordController');
var clientVehiclesCtrl = rootRequire('app/controllers/client/vehiclesController');
var clientSchedulesCtrl = rootRequire('app/controllers/client/schedulesController');
var clientDashboardCtrl = rootRequire('app/controllers/client/dashboardController');
var clientSosCtrl = rootRequire('app/controllers/client/sosController');

var publicWelcomeCtrl = rootRequire('app/controllers/public/welcomeController');
var publicClientsCtrl = rootRequire('app/controllers/public/clientsController');
var publicSessionsCtrl = rootRequire('app/controllers/public/sessionsController');
var publicStaffCtrl = rootRequire('app/controllers/public/staffController');

var mechanicDashboardCtrl = rootRequire('app/controllers/mechanic/dashboardController');
var mechanicAutoPartsCtrl = rootRequire('app/controllers/mechanic/autoPartsController');
var mechanicServicesCtrl = rootRequire('app/controllers/mechanic/servicesController');
var mechanicMechanicCtrl = rootRequire('app/controllers/mechanic/mechanicController');
var mechanicSosCtrl = rootRequire('app/controllers/mechanic/sosController');
var mechanicPasswordCtrl = rootRequire('app/controllers/mechanic/passwordController');

var apiSchedulesCtrl = rootRequire('app/controllers/api/schedulesController');

var express = require('express');

var adminNamespace = express.Router();
var attendantNamespace = express.Router();
var publicNamespace = express.Router();
var clientNamespace = express.Router();
var technicianNamespace = express.Router();
var mechanicNamespace = express.Router();
var apiNamespace = express.Router();

function initAppRoutes(app) {
  app.use(publicNamespace.use('/',
    publicWelcomeCtrl,
    publicSessionsCtrl,
    publicStaffCtrl,

    publicClientsCtrl
  ));

  app.use(apiNamespace.use('/api',
    apiSchedulesCtrl
  ));

  app.use(clientNamespace.use('/client-panel',
    redirectIfNotClient,
    setCurrentClient,

    clientClientCtrl,
    clientVehiclesCtrl,
    clientSchedulesCtrl,
    clientDashboardCtrl,
    clientSosCtrl,
    clientPasswordCtrl
  ));

  app.use(adminNamespace.use('/admin-panel',
    requireAuthentication,
    redirectIfNotAdmin,
    setCurrentStaff,

    adminDashboardCtrl,
    adminStaffsCtrl,
    adminTeamsCtrl,
    adminServicesCtrl,
    adminAutoPartsCtrl,
    adminClientsCtrl,
    adminVehiclesCtrl,
    adminReportsCtrl,
    adminAdminCtrl,
    adminPasswordCtrl,
    adminSosCtrl,
    adminSchedulesCtrl,
    adminRevisionsCtrl
  ));

  app.use(attendantNamespace.use('/attendant-panel',
    requireAuthentication,
    redirectIfNotAttendant,
    setCurrentStaff,

    attendantDashboardCtrl,
    attendantClientsCtrl,
    attendantVehiclesCtrl,
    attendantSchedulesCtrl,
    attendantAttendantCtrl,
    attendantPasswordCtrl,
    attendantSosCtrl
  ));

  app.use(technicianNamespace.use('/technician-panel',
    requireAuthentication,
    redirectIfNotTechnician,
    setCurrentStaff,

    technicianDashboardCtrl,
    technicianSosCtrl,
    technicianClientsCtrl,
    technicianAutoPartsCtrl,
    technicianServicesCtrl,
    technicianSchedulesCtrl,
    technicianRevisionsCtrl,
    technicianTeamsCtrl,
    technicianTechnicianCtrl,
    technicianPasswordCtrl
  ));

  app.use(mechanicNamespace.use('/mechanic-panel',
    requireAuthentication,
    redirectIfNotMechanic,
    setCurrentStaff,

    mechanicDashboardCtrl,
    mechanicAutoPartsCtrl,
    mechanicServicesCtrl,
    mechanicMechanicCtrl,
    mechanicSosCtrl,
    mechanicPasswordCtrl
  ));
};

function requireAuthentication(req, res, next) {
  if (req.user && req.user.isStaff) {
    next();
  }
  else {
    res.redirect('/');
  }

};

function redirectIfNotClient(req, res, next) {
  if (req.user && req.user.isClient) {
    next();
  } else {
    res.redirect('/login');
  }
};

function setCurrentClient(req, res, next) {
  req.currentClient = req.user;
  next();
};

function redirectIfNotAdmin(req, res, next) {
  if (req.user.role == "Gerente") {
    next();
  } else {
    res.redirect('/');
  }
};

function setCurrentStaff(req, res, next) {
  req.currentStaff = req.user;
  next();
};

function redirectIfNotAttendant(req, res, next) {
  if (req.user.role == "Atendente") {
    next();
  } else {
    res.redirect('/');
  }
};

function redirectIfNotTechnician(req, res, next) {
  if (req.user.role == "Técnico") {
    next();
  } else {
    res.redirect('/');
  }
};

function redirectIfNotMechanic(req, res, next) {
  if (req.user.role == "Mecânico") {
    next();
  } else {
    res.redirect('/');
  }
};

module.exports.initAppRoutes = initAppRoutes;