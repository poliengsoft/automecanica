var flash = require('connect-flash');

module.exports = function(app) {
  app.use(flash());
  app.use(function (req, res, next) {
    res.locals.flash = req.flash.bind(req);
    next();
  });
  app.use(function (req, res, next) {
    req.flashNotFound = flashNotFound;
    req.flashCreateError = flashCreateError;
    req.flashCreateSuccess = flashCreateSuccess;
    req.flashUpdateError = flashUpdateError;
    req.flashUpdateSuccess = flashUpdateSuccess;
    req.flashDeleteError = flashDeleteError;
    req.flashDeleteSuccess = flashDeleteSuccess;
    req.flashLoginError = flashLoginError;
    req.flashLoginSuccess = flashLoginSuccess;
    req.flashPaymentError = flashPaymentError;
    req.flashPaymentSuccess = flashPaymentSuccess;
    req.flashDifferentPasswords = flashDifferentPasswords;
    next();
  });
};

function flashNotFound(resource) {
  this.flash('error', resource + ' não encontrado');
};

function flashCreateError(resource) {
  this.flash('error', 'Falha ao criar o ' + resource);
};

function flashCreateSuccess(resource) {
  this.flash('success', resource + ' criado com sucesso');
};

function flashUpdateError(resource) {
  this.flash('error', 'Falha ao editar o ' + resource);
};

function flashUpdateSuccess(resource) {
  this.flash('success', resource + ' editado com sucesso');
};

function flashDeleteError(resource) {
  this.flash('error', 'Falha ao deletar o ' + resource);
};

function flashDeleteSuccess(resource) {
  this.flash('success', resource + ' deletado com sucesso');
};

function flashLoginError() {
  this.flash('error', 'Falha ao tentar se logar');
};

function flashLoginSuccess() {
  this.flash('success', 'Usuário logado com sucesso');
};

function flashPaymentError() {
  this.flash('error', 'Nâo foi possível atualizar o status do pagamento');
};

function flashPaymentSuccess() {
  this.flash('success', 'Pagamento confirmado com sucesso');
};

function flashDifferentPasswords() {
  this.flash('error', 'A senha e a sua confirmação estão diferentes!');
}