var session = require('express-session');
var FileStore = require('session-file-store')(session);

module.exports = function(app) {
  app.use(session({
    store: new FileStore(),
    secret: process.env.APP_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 60 * 60 * 1000 // In milisseconds
    }
  }));
};