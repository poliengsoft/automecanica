var path = require('path');

module.exports = function(app) {
  app.set('views', path.join(__dirname, '..', 'app/views'));
  app.set('view engine', 'jade');
  app.set('view options', {
    layout: false,
    pretty: true,
    compileDebug: true
  });
};