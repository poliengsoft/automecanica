var moment = require('moment');

module.exports = function(app) {
  moment.locale('pt-BR');
  app.use(setMoment);
};


function setMoment(req, res, next) {
  res.locals.moment = moment;
  next();
}