var passport = require('passport');
var requireAll = require('require-all');
var path = require('path');

module.exports = function(app) {
  app.use(passport.initialize());
  app.use(passport.session());

  requireAll(path.join(__dirname, '..', 'passport_strategies'));
};