module.exports = [
  setBaseUrl,
  setUser
];

function setBaseUrl(req, res, next) {
  Object.defineProperty(res.locals, 'baseUrl', {
    enumerable: true,
    configurable: false,
    get: function () { return req.baseUrl; }
  });
  next();
};

function setUser(req, res, next) {
  if (req.user) {
    res.locals.user = req.user;
  };
  next();
}