require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE clients\
      (\
         id SERIAL PRIMARY KEY,\
         name VARCHAR(255) NOT NULL,\
         email VARCHAR(255) NOT NULL,\
         address VARCHAR(255) NOT NULL,\
         password VARCHAR(255) NOT NULL,\
         telephone1 VARCHAR(255),\
         telephone2 VARCHAR(255)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS clients;',
    function(err) {
      if (err) throw err;
      next();
    });
};
