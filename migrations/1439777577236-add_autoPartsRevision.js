require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE autoPartsRevisions\
      (\
        revisionId INTEGER REFERENCES revisions(id),\
        autoPartId INTEGER REFERENCES autoParts(id),\
        quantity INTEGER NOT NULL,\
        PRIMARY KEY (revisionId, autoPartId)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS autoPartsRevisions;',
    function(err) {
      if (err) throw err;
      next();
    });
};
