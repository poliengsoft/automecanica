require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE sos\
      (\
        id SERIAL PRIMARY KEY,\
        tasksStatus VARCHAR(255) NOT NULL,\
        paymentStatus VARCHAR(255) NOT NULL,\
        price VARCHAR(255) NOT NULL,\
        dueDate DATE NOT NULL,\
        endDate DATE NOT NULL,\
        withdrawalDate DATE NOT NULL,\
        cancellationReason VARCHAR(255) NOT NULL,\
        vehicleId INTEGER NOT NULL REFERENCES vehicles(id)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS sos;',
    function(err) {
      if (err) throw err;
      next();
    });
};
