require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery('\
    CREATE TRIGGER triggerConcludedSO\
    BEFORE UPDATE ON sos\
    FOR EACH ROW EXECUTE PROCEDURE checkConcludedSO();\
  ', function(err) {
      if (err) throw err;
      next();
  });
};

exports.down = function(next) {
  daoConnector.textQuery('\
    DROP TRIGGER triggerConcludedSO ON sos;\
    ', function(err) {
        if (err) throw err;
        next();
    });
};
