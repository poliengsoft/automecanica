require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE tasks\
      (\
         serviceId INTEGER REFERENCES services(id),\
         soId INTEGER REFERENCES sos(id),\
         status VARCHAR(255) NOT NULL,\
         price REAL NOT NULL,\
         PRIMARY KEY(soId, serviceId)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS tasks;',
    function(err) {
      if (err) throw err;
      next();
    });
};
