require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE teamMechanic(\
      mechanicId INTEGER NOT NULL,\
      mechanicId2 INTEGER NOT NULL,\
      teamId INTEGER NOT NULL,\
      PRIMARY KEY (mechanicId, teamId),\
      CONSTRAINT teamIdFkey FOREIGN KEY (teamId)\
        REFERENCES teams (id) ON DELETE CASCADE,\
      CONSTRAINT mechanicIdFkey FOREIGN KEY (mechanicId)\
        REFERENCES staffs (id) ON DELETE CASCADE,\
      CONSTRAINT mechanicId2Fkey FOREIGN KEY (mechanicId2)\
        REFERENCES staffs (id) ON DELETE CASCADE\
    );',
    function(err) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS teamMechanic',
    function(err, result) {
      if (err) throw err;
      next();
    });
};
