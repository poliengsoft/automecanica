require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery('\
    CREATE TRIGGER triggerSetCreatedAt\
    BEFORE INSERT ON sos\
    FOR EACH ROW EXECUTE PROCEDURE setCreatedAt();\
  ', function(err) {
      if (err) throw err;
      next();
  });
};

exports.down = function(next) {
  daoConnector.textQuery('\
    DROP TRIGGER triggerSetCreatedAt ON sos;\
    ', function(err) {
        if (err) throw err;
        next();
    });
};
