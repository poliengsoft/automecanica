require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE concludedSOs\
      (\
         soId INTEGER REFERENCES sos(id),\
         memberId INTEGER REFERENCES staffs(id),\
         PRIMARY KEY (soId, memberId)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS concludedSOs;',
    function(err) {
      if (err) throw err;
      next();
    });
};
