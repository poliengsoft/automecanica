require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery('\
    CREATE OR REPLACE FUNCTION checkConcludedSO()\
    RETURNS trigger AS $checkConcludedSO$\
    DECLARE\
      curs CURSOR FOR\
        SELECT mechanic1Id AS id from teamMechanic WHERE teamId in\
          (SELECT teamId from teamsSo WHERE soId = NEW.id)\
        UNION\
        SELECT mechanic2Id from teamMechanic WHERE teamId in\
          (SELECT teamId from teamsSo WHERE soId = NEW.id);\
    BEGIN\
      IF NEW.paymentStatus = \'Confirmado\' AND\
      NEW.tasksStatus = \'Finalizada\' THEN\
        NEW.isFinished = true;\
        \
        FOR row IN curs LOOP\
          INSERT INTO concludedSOs VALUES (NEW.id, row.id);\
        END LOOP;\
        \
      ELSE\
        NEW.isFinished = false;\
      END IF;\
      RETURN NEW;\
    END;\
    $checkConcludedSO$ LANGUAGE plpgsql;\
  ', function(err, result) {
      if (err) throw err;
      next();
  });
};

exports.down = function(next) {
  daoConnector.textQuery('\
    DROP FUNCTION IF EXISTS checkConcludedSO();\
  ', function(err) {
        if (err) throw err;
        next();
  });
};
