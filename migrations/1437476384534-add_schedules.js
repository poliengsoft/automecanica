require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE schedules\
      (\
        id SERIAL PRIMARY KEY,\
        day DATE,\
        startTime TIME,\
        clientId INTEGER\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS schedules;',
    function(err) {
      if (err) throw err;
      next();
    });
};
