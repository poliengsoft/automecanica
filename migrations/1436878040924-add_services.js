require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE services\
      (\
         id SERIAL PRIMARY KEY,\
         name VARCHAR(255) NOT NULL,\
         price REAL NOT NULL\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS services;',
    function(err) {
      if (err) throw err;
      next();
    });
};
