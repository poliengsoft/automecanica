require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos\
      ADD createdAt timestamp;',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos\
      DROP createdAt;',
    function(err) {
      if (err) throw err;
      next();
    });
};
