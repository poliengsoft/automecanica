require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE schedules \
      DROP day,\
      DROP startTime,\
      DROP clientId,\
      ADD datetime timestamp UNIQUE,\
      ADD vehicleId INTEGER;',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE schedules\
      DROP datetime,\
      DROP vehicleId,\
      ADD day DATE,\
      ADD startTime TIME,\
      ADD clientId INTEGER;',
    function(err) {
      if (err) throw err;
      next();
    });
};
