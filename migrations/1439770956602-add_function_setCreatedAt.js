require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery('\
    CREATE OR REPLACE FUNCTION setCreatedAt()\
    RETURNS trigger AS $setCreatedAt$\
    BEGIN\
        NEW.createdAt = NOW();\
      RETURN NEW;\
    END;\
    $setCreatedAt$ LANGUAGE plpgsql;\
  ', function(err, result) {
      if (err) throw err;
      next();
  });
};

exports.down = function(next) {
  daoConnector.textQuery('\
    DROP FUNCTION IF EXISTS setCreatedAt();\
  ', function(err) {
        if (err) throw err;
        next();
  });
};
