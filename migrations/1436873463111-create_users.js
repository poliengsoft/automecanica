require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE users ( id SERIAL PRIMARY KEY, name VARCHAR(255) );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS users;',
    function(err) {
      if (err) throw err;
      next();
    });
};
