require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.transaction(function (err, dbClient, done) {
    dbClient.query('ALTER TABLE teamMechanic\
      RENAME COLUMN mechanicid TO mechanic1id;',
    function(err) {
      if (err) throw err;
      dbClient.query(
        'ALTER TABLE teamMechanic\
          RENAME COLUMN mechanicid2 TO mechanic2id;',
        function(err) {
          if (err) throw err;
          done(null, function() {
            next();
          });
        });
    });
  });
};

exports.down = function(next) {
  daoConnector.transaction(function (err, dbClient, done) {
    dbClient.query('ALTER TABLE teamMechanic\
      RENAME COLUMN mechanic1id TO mechanicid;',
    function(err, result) {
      if (err) throw err;
      dbClient.query(
        'ALTER TABLE teamMechanic\
          RENAME COLUMN mechanic2id TO mechanicid2;',
        function(err, result) {
          if (err) throw err;
          done(null, function() {
            next();
          });
        });
    });
  });
};
