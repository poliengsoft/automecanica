require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE soAutoParts\
      (\
         soId INTEGER REFERENCES sos(id),\
         autoPartId INTEGER REFERENCES autoParts(id),\
         quantity INTEGER NOT NULL,\
         unitPrice REAL NOT NULL,\
         PRIMARY KEY(soId, autoPartId)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS soAutoParts;',
    function(err) {
      if (err) throw err;
      next();
    });
};
