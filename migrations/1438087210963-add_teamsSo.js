require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE teamsSo\
      (\
         teamId INTEGER REFERENCES teams(id),\
         soId INTEGER REFERENCES sos(id)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS teamsSo;',
    function(err) {
      if (err) throw err;
      next();
    });
};
