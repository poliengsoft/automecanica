require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE vehicles\
      (\
         id SERIAL PRIMARY KEY,\
         model VARCHAR(255) NOT NULL,\
         licensePlate VARCHAR(255) NOT NULL,\
         renavam VARCHAR(255) NOT NULL,\
         year INTEGER NOT NULL\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS vehicles;',
    function(err) {
      if (err) throw err;
      next();
    });
};
