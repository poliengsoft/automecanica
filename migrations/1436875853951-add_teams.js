require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE teams\
      (\
        id SERIAL PRIMARY KEY,\
        speciality TEXT NOT NULL\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS teams;',
    function(err) {
      if (err) throw err;
      next();
    });
};
