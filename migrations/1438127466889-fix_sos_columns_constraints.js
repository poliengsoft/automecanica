require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE IF EXISTS sos\
      ALTER COLUMN withdrawalDate DROP NOT NULL,\
      ALTER COLUMN endDate DROP NOT NULL,\
      ALTER COLUMN cancellationReason DROP NOT NULL;\
    ',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE IF EXISTS sos\
      ALTER COLUMN withdrawalDate SET NOT NULL,\
      ALTER COLUMN endDate SET NOT NULL,\
      ALTER COLUMN cancellationReason SET NOT NULL;\
    ',
    function(err, result) {
      if (err) throw err;
      next();
    });
};
