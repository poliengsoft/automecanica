require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE revisions\
      (\
        id SERIAL PRIMARY KEY,\
        name VARCHAR(255) NOT NULL,\
        description TEXT\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS revisions;',
    function(err) {
      if (err) throw err;
      next();
    });
};
