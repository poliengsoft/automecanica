require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE vehicles ' + 
    'ADD clientId INTEGER NOT NULL REFERENCES clients(id)',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE vehicles ' + 
    'DROP clientId',
    function(err) {
      if (err) throw err;
      next();
    });
};
