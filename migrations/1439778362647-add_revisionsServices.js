require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'CREATE TABLE revisionsServices\
      (\
        revisionId INTEGER REFERENCES revisions(id),\
        serviceId INTEGER REFERENCES services(id),\
        PRIMARY KEY (revisionId, serviceId)\
      );',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'DROP TABLE IF EXISTS revisionsServices;',
    function(err) {
      if (err) throw err;
      next();
    });
};
