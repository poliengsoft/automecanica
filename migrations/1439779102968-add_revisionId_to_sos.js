require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos ' + 
    'ADD revisionId INTEGER REFERENCES revisions(id)',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos ' + 
    'DROP revisionId',
    function(err) {
      if (err) throw err;
      next();
    });
};
