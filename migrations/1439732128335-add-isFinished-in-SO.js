require('dotenv').load();
var daoConnector = require('./daoConnector');

exports.up = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos \
    ADD isFinished BOOL \
    DEFAULT false',
    function(err, result) {
      if (err) throw err;
      next();
    });
};

exports.down = function(next) {
  daoConnector.textQuery(
    'ALTER TABLE sos DROP isFinished',
    function(err) {
      if (err) throw err;
      next();
    });
};
