require('dotenv').load();
var staffsDAO = require('../app/daos/staffsDAO');

var admin = {
  name: 'adm',
  email: 'adm@adm.com',
  address: 'adm',
  password: '$2a$08$IPQ.pXVFnZuzXTHv75KnHuLO894IsfXnvX2HfkyREIg6KcHfjBcla', // adm
  role: 'Gerente',
  telephone1: 'adm'
};

staffsDAO.insert(admin, function (err, createdAdmin) {
  if (err) throw err;
  console.log("Admin created!");
  console.log("User: adm");
  console.log("Password: adm");
});