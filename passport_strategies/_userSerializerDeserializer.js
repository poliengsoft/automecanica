var passport = require('passport');
var clientsDAO = rootRequire('app/daos/clientsDAO');
var staffsDAO = rootRequire('app/daos/staffsDAO');

passport.serializeUser(function(user, done) {
  var serializedUser = JSON.stringify({
    id: user.id,
    type: user.type
  });
  done(null, serializedUser);
});

passport.deserializeUser(function(serializedUser, done) {
  var deserializedUser = JSON.parse(serializedUser);
  if (deserializedUser.type === 'Client') {
    clientsDAO.selectById(deserializedUser.id, function(err, user) {
      done(err, user);
    });
  } else if(deserializedUser.type === 'Staff') {
    staffsDAO.selectById(deserializedUser.id, function(err, user) {
      done(err, user);
    });
  }
});
