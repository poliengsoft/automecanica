var passport = require('passport');

var LocalStrategy = require('passport-local').Strategy;
var staffsDAO = rootRequire('app/daos/staffsDAO');
var passwordManager = rootRequire('app/services/passwordManager');

passport.use('staff-local', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {

    staffsDAO.selectByEmail(email, function(err, staff) {
      if (err) return done(err);
      if (!staff) return done(null, false, { message: 'Email incorreto' });
      passwordManager.checkPassword(password, staff.password,
        function(err, isPasswordValid) {
          if (err || !isPasswordValid) {
            return done(null, false, { message: 'Senha incorreta' });
          }
          else {
            return done(null, staff);
          }
        });
    });

  })
);
