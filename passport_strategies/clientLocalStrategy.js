var passport = require('passport');

var LocalStrategy = require('passport-local').Strategy;
var clientsDAO = rootRequire('app/daos/clientsDAO');
var passwordManager = rootRequire('app/services/passwordManager');

passport.use('client-local', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {

    clientsDAO.selectByEmail(email, function(err, client) {
      if (err) return done(err);
      if (!client) return done(null, false, { message: 'Email incorreto' });
      passwordManager.checkPassword(password, client.password,
        function(err, isPasswordValid) {
          if (err || !isPasswordValid) {
            return done(null, false, { message: 'Senha incorreta' });
          }
          else {
            return done(null, client);
          }
        });
    });

  })
);