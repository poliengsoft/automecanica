var express = require('express');

var jadeInit = require('./initializers/jade');
var assetsInit = require('./initializers/assets');
var loggerInit = require('./initializers/morgan');
var cookieInit = require('./initializers/cookie');
var sessionInit = require('./initializers/session');
var flashInit = require('./initializers/flash');
var parserInit = require('./initializers/parser');
var passportInit = require('./initializers/passport');
var momentInit = require('./initializers/moment');

var methodOverride = require('./middlewares/method_override');
var setLocals = require('./middlewares/set_locals');

var routes = require('./routes');

var app = express();

// Initializers
jadeInit(app);
assetsInit(app);
loggerInit(app);
parserInit(app);
cookieInit(app);
sessionInit(app);
flashInit(app);

passportInit(app);

momentInit(app);

// Middlewares
app.use(methodOverride);
app.use(setLocals);

// Routes
routes.initAppRoutes(app);

// No route found
app.use(function(req, res, next) {
  res.redirect('/not-found/404.html');
});

// Generic Server Error Handler
app.use(function(err, req, res, next) {
  err.status = err.status || 500;
  console.log('Request: ' + req.url + ' got');
  console.log(err.stack);
  res.locals.err = err;
  res.locals.ENV = process.env.ENV;
  try {
    res.render('public/error/500');
  } catch(e) {
    // Ignoring error, to always see errors on log,
    // but to not see 'Cannont set headers...'
  }
});

module.exports = app;