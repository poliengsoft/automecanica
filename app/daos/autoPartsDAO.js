var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var AutoPart = require('../model/autoPart');

function validateRequiredFields(autoPart) {
  if (autoPart.name == '' ||
            autoPart.price == '') {
    autoPart.error = new Error("Campo obrigatório vazio!");
  }
}

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(AutoPart);
  
  rowFixer.rename('autopartid', 'autoPartId');
  rowFixer.rename('soid', 'soId');
  rowFixer.rename('unitprice', 'unitPrice');
}

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT * FROM autoParts;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allBySOId = function(soId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT soAutoParts.*, autoParts.name\
      FROM soAutoParts LEFT JOIN autoParts\
      ON (soAutoParts.autoPartId = autoParts.id)\
      WHERE soid = $1;',
    [soId],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.selectAllByIds = function(idsList, fn) {
  if (!idsList) return fn(null, []);
  var ids = ( idsList instanceof Array ) ? idsList : [idsList];
  ids = ids.map(function(id) { return Number(id); });

  daoConnector.paramaterizedQuery(
    'SELECT *\
      FROM autoParts\
      WHERE ARRAY[id] && ($1);', [ids],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.insert = function(autoPart, fn) {
  validateRequiredFields(autoPart);
  if (autoPart.error) {
    return fn(autoPart.error);
  }
  daoConnector.paramaterizedQuery(
    'INSERT INTO autoParts (name, price) VALUES ($1, $2) RETURNING *;',
    [autoPart.name, autoPart.price],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM autoParts WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(autoPart, fn) {
  validateRequiredFields(autoPart);
  if (autoPart.error) {
    return fn(autoPart.error);
  }
  daoConnector.paramaterizedQuery('UPDATE autoParts SET name=$2, price=$3 WHERE id=$1;',
    [autoPart.id, autoPart.name, autoPart.price],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        autoPart.__proto__ = AutoPart.prototype;
        fn(null, autoPart);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM autoParts WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.selectByListOfIds = function(idList, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT *\
      FROM autoParts\
      where ARRAY[id] && $1;',
      [idList],
      function(err, result) {
        if (err) return fn(err);
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      });
};

module.exports.allByRevisionId = function (id, fn) {
  daoConnector.paramaterizedQuery('\
    SELECT * FROM autoParts WHERE id in\
    (SELECT autoPartId FROM autoPartsRevisions WHERE\
    revisionId = $1);\
    ', [id], function (err, result) {
      if (err) {
        return fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};
