var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Staff = require('../model/staff');

function validateRequiredFields(staff) {
  if (staff.name == '' ||
      staff.email == '' ||
        staff.address == '' ||
          staff.role == '' ||
            staff.telephone1 == '') {
    staff.error = new Error("Campo obrigatório vazio!");
  }
}

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT * FROM staffs ORDER BY id;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(function(row) {
        row.__proto__ = Staff.prototype;
      });
      fn(null, result.rows);
    }
  });
};

module.exports.insert = function(staff, fn) {
  validateRequiredFields(staff);
  if (staff.error) {
    return fn(staff.error);
  }
  daoConnector.paramaterizedQuery(
    'INSERT INTO staffs' + 
    ' (name, email, address, password, role, telephone1, telephone2)' +
    ' VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *;', 
    [
      staff.name,
      staff.email,
      staff.address,
      staff.password,
      staff.role,
      staff.telephone1,
      staff.telephone2
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Staff.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM staffs WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Staff.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(staff, fn) {
  validateRequiredFields(staff);
  if (staff.error) {
    return fn(staff.error);
  }
  daoConnector.paramaterizedQuery('UPDATE staffs\
  SET name=$2, email=$3, address=$4,\
  role=$5, telephone1=$6, telephone2=$7 WHERE id=$1;',
    [
      staff.id,
      staff.name,
      staff.email,
      staff.address,
      staff.role,
      staff.telephone1,
      staff.telephone2
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        staff.__proto__ = Staff.prototype;
        fn(null, staff);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM staffs WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.selectByEmail = function(email, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM staffs WHERE email=$1;', [email],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Staff.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.updatePassword = function(staff, fn) {
  daoConnector.paramaterizedQuery(
    'UPDATE staffs SET\
      password=$2 WHERE id=$1;',
    [
      staff.id,
      staff.password
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        staff.__proto__ = Staff.prototype;
        fn(null, staff);
      }
    });
};
