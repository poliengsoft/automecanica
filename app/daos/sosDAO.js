var daoConnector = require('./daoConnector');
var Service = require('../model/service');
var ObjectFixer = require('../services/objectFixer');
var SO = require('../model/so');
var moment = require('moment');

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(SO);

  rowFixer.rename('duedate', 'dueDate');
  rowFixer.rename('enddate', 'endDate');
  rowFixer.rename('withdrawaldate', 'withdrawalDate');
  rowFixer.rename('licenseplate','licensePlate');
  rowFixer.rename('tasksstatus', 'tasksStatus');
  rowFixer.rename('paymentstatus', 'paymentStatus');
  rowFixer.rename('cancellationreason', 'cancellationReason');
  rowFixer.rename('vehicleid', 'vehicleId');
  rowFixer.rename('isfinished', 'isFinished');
  rowFixer.rename('createdat', 'createdAt');

  rowFixer.fix('dueDate', toMomentDate);
  rowFixer.fix('endDate', toMomentDate);
  rowFixer.fix('withdrawalDate', toMomentDate);
  rowFixer.fix('createdAt', toMomentDate);

  rowFixer.fix('price', parseFloat);
}

function toMomentDate(date) {
  return date ? moment(date) : null;
}

module.exports.updatePaymentStatus = function(soId, fn) {
  daoConnector.paramaterizedQuery('UPDATE sos SET paymentStatus=\'Confirmado\'\
    WHERE id=$1', [soId], function(err, result) {
      if (err) {
        return fn(err);
      }
      else {
        var wasUpdate = result.rowCount > 0;
        return fn(null, wasUpdate);
      }
    });
};

module.exports.allWithCarPlateFromMechanicId = function(mechanicId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT sos.*, licensePlate FROM sos\
     JOIN vehicles ON (vehicleId = vehicles.id)\
     WHERE sos.id IN (SELECT soid FROM teamsso WHERE teamId IN\
     (SELECT teamId from teamMechanic WHERE\
     mechanic1Id = $1 OR mechanic2Id = $1)) ORDER BY sos.id;',
     [mechanicId], function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allWithCarPlate = function(fn) {
  daoConnector.textQuery(
    'SELECT sos.*, vehicles.licensePlate FROM sos\
     JOIN vehicles ON (vehicleId = vehicles.id)\
     ORDER BY sos.id DESC;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allByClientId = function(clientId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT *\
      FROM vehicles RIGHT JOIN sos\
      ON sos.vehicleId = vehicles.id\
      WHERE clientId = $1;',
     [clientId],
     function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.allBetween = function(startDate, endDate, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT *\
      FROM sos\
      WHERE createdAt > $1 AND createdAt < $2;',
      [startDate, endDate],
      function(err, result) {
        if (err) return fn(err);
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      });
};

module.exports.insert = function(so, fn) {
  validateSO(so);
  if (so.error) {
    return fn(so.error);
  }
  var osStatus = so.cancellationReason ? 'Cancelado' : 'Pendente';

  daoConnector.transaction(function(err, dbClient, done) {
    if (err) return done(err, fn);
    dbClient.query('INSERT INTO sos (tasksStatus, paymentStatus,\
      price, dueDate, cancellationReason, vehicleId)\
      values ($1, $2, $3, $4, $5, $6) RETURNING *',
      [osStatus, osStatus, so.price, so.dueDate, so.cancellationReason,
      so.vehicleId], function (err, result) {
        if (err) return done(err, fn);
        so.id = result.rows[0].id;

        fillTeamsSORelation(so, dbClient, done, fn);
      }
    );
  });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT * FROM sos WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
}

function fillTeamsSORelation(so, dbClient, done, fn) {
  if (!so.cancellationreason) {
    var query = buildTeamsSOQuery(so.id, so.teams)
    if (!query) {
      return done(new Error('There is insufficient data to build query'), fn);
    }
    else {
      dbClient.query(query, function (err, result) {
        if (err) return done(err, fn);
        else {
          fillAutoPartsSORelation(so, dbClient, done, fn);
        }
      });
    }
  }
  else {
    fillAutoPartsSORelation(so, dbClient, done, fn);
  }
}

function fillAutoPartsSORelation(so, dbClient, done, fn) {
  var query = buildAutoPartsSOQuery(so.id, so.autoParts);
  if (!query) {
    return fillTasksSORelation(so, dbClient, done, fn);
  }
  else {
    dbClient.query(query, function (err, result) {
      if (err) return done(err, fn);
      else {
        fillTasksSORelation(so, dbClient, done, fn);
      }
    });
  }
}

function fillTasksSORelation(so, dbClient, done, fn) {
  var query = buildTasksSOQuery(so.id, so.tasks);
  if (so.cancellationReason) {
    query = buildEndedTasksSOQuery(so.id, so.tasks);
  };
  dbClient.query(query, function (err, result) {
    if (err) return done(err, fn);
    else {
      done(null, function() {
        fn(null, so);
      });
    }
  });
}

function buildTeamsSOQuery(soId, teams) {
  if (!teams) return '';
  var query  = 'INSERT INTO teamsSO (teamId, soId) VALUES ';
  var queryValuesList = [];
  teams.forEach(function (team) {
    queryValuesList.push( buildQueryValues(team.id, soId) );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildAutoPartsSOQuery(soId, autoParts) {
  if (autoParts.length == 0) return '';
  var query  = 'INSERT INTO soAutoParts (soId, autoPartId, quantity, unitPrice)\
  VALUES ';
  var queryValuesList = [];
  autoParts.forEach(function (autoPart) {
    queryValuesList.push(
      buildQueryValues(soId, autoPart.id, autoPart.quantity, autoPart.price)
      );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildTasksSOQuery(soId, tasks) {
  var query  = 'INSERT INTO tasks (serviceId, soId, status, price) VALUES ';
  var queryValuesList = [];
  tasks.forEach(function (task) {
    queryValuesList.push(
      buildQueryValues(task.id, soId, '\'Pendente\'', task.price)
      );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildEndedTasksSOQuery(soId, tasks) {
  var query  = 'INSERT INTO tasks (serviceId, soId, status, price) VALUES ';
  var queryValuesList = [];
  tasks.forEach(function (task) {
    queryValuesList.push(
      buildQueryValues(task.id, soId, '\'Finalizada\'', task.price)
      );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildQueryValues() {
  return '(' + Array.prototype.join.call(arguments, ', ') + ')';
}

function validateSO(so, fn) {
  if (!so.dueDate) {
    so.error = new Error("A data de previsão de entrega é obrigatória");
  }
  else if (so.tasks.length == 0) {
    so.error = new Error("É obrigatório escolher ao menos um serviço");
  }
}
