var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Vehicle = require('../model/vehicle');

function validateRequiredFields(vehicle) {
  if (vehicle.model == '' ||
      vehicle.licensePlate == '' ||
            vehicle.renavam == '') {
    vehicle.error = new Error("Campo obrigatório vazio!");
  }
}

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(Vehicle);

  rowFixer.rename('licenseplate', 'licensePlate');
  rowFixer.rename('clientid', 'clientId');
  rowFixer.rename('ownerid', 'ownerId');
};

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT v.*, c.name as owner, c.id as ownerId\
    FROM vehicles v \
    LEFT JOIN clients c ON v.clientid = c.id;', function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
  });
};

module.exports.allByClientId = function(clientId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT * FROM vehicles WHERE clientId = $1;',
    [clientId], function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
  });
};

module.exports.insert = function(vehicle, fn) {
  validateRequiredFields(vehicle);
  if (vehicle.error) {
    return fn(vehicle.error);
  }
  daoConnector.paramaterizedQuery(
    'INSERT INTO vehicles' + 
    ' (model, licensePlate, renavam, year, clientId)' +
    ' VALUES ($1, $2, $3, $4, $5) RETURNING *;', 
    [
      vehicle.model,
      vehicle.licensePlate,
      vehicle.renavam,
      vehicle.year,
      vehicle.clientId
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM vehicles WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(vehicle, fn) {
  validateRequiredFields(vehicle);
  if (vehicle.error) {
    return fn(vehicle.error);
  }
  daoConnector.paramaterizedQuery(
    'UPDATE vehicles SET ' + 
    'model = $2, licensePlate = $3, renavam = $4, year = $5 ' +
    'WHERE id=$1 and clientId = $6;',
    [
      vehicle.id,
      vehicle.model,
      vehicle.licensePlate,
      vehicle.renavam,
      vehicle.year,
      vehicle.clientId
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        standardizeRow(vehicle);
        fn(null, vehicle);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM vehicles WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};
