var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Service = require('../model/service');
  var moment = require('moment');

function validateRequiredFields(service) {
  if (service.name == '' ||
            service.price == '') {
    service.error = new Error("Campo obrigatório vazio!");
  }
}

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(Service);

  rowFixer.rename('serviceid', 'serviceId');
  rowFixer.rename('soid', 'soId');
}

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT * FROM services;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allBySOId = function(soId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT tasks.*, services.name\
      FROM tasks LEFT JOIN services\
      ON (tasks.serviceid = services.id)\
      WHERE soid = $1;',
    [soId],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.selectAllByIds = function(idsList, fn) {
  if (!idsList) return fn(null, []);
  var ids = ( idsList instanceof Array ) ? idsList : [idsList];
  ids = ids.map(function(id) { return Number(id); });
  daoConnector.paramaterizedQuery(
    'SELECT *\
      FROM services\
      WHERE ARRAY[id] && $1;',
      [ids],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.insert = function(service, fn) {
  validateRequiredFields(service);
  if (service.error) {
    return fn(service.error);
  }
  daoConnector.paramaterizedQuery(
    'INSERT INTO services (name, price) VALUES ($1, $2) RETURNING *;',
    [service.name, service.price],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM services WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(service, fn) {
  validateRequiredFields(service);
  if (service.error) {
    return fn(service.error);
  }
  daoConnector.paramaterizedQuery('UPDATE services SET name=$2, price=$3 WHERE id=$1;',
    [service.id, service.name, service.price],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        service.__proto__ = Service.prototype;
        fn(null, service);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM services WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.selectByListOfIds = function(idList, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT * FROM services\
      where ARRAY[id] && ($1);',
      [idList], function(err, result) {
      if (err) return fn(err);
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    });
};

module.exports.updateTasksFromSO = function(so, fn) {
  daoConnector.transaction(function(err, dbClient, done) {
  var query = 'UPDATE tasks SET status = \'Finalizada\'\
    WHERE soId = $1 AND ARRAY[serviceId] && $2;';
    dbClient.query(query, [so.id, so.tasksIds], function (err, result) {
      if (err) return done(err, fn);
      dbClient.query('SELECT COUNT(*) FROM tasks\
        WHERE soid = $1 AND status = \'Pendente\'', [so.id], function(err, result) {
          if (err) return done(err, fn);
          var count = result.rows[0].count;
          if (count == 0) {
            dbClient.query('UPDATE sos SET tasksStatus = \'Finalizada\', endDate = CURRENT_DATE\
              WHERE id = $1;', [so.id], function (err, result) {
                if (err) return done(err, fn);
                return done(null, fn);
            });
          }
          else {
            done(null, fn);
          }
        });
    });
  });
};

module.exports.allByRevisionId = function (id, fn) {
  daoConnector.paramaterizedQuery('\
    SELECT * FROM services WHERE id in\
    (SELECT serviceId FROM revisionsServices WHERE\
    revisionId = $1);\
    ', [id], function (err, result) {
      if (err) {
        return fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};
