var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Team = require('../model/team');
var Staff = require('../model/staff');

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(Team);

  rowFixer.rename('teamid', 'teamId');
  rowFixer.rename('mechanic1id', 'mechanic1Id');
  rowFixer.rename('mechanic2id', 'mechanic2Id');
  rowFixer.rename('mechanic1name', 'mechanic1Name');
  rowFixer.rename('mechanic2name', 'mechanic2Name');
  rowFixer.rename('totalosperteam', 'totalOSPerTeam');
};

function validateRequiredFields(team) {
  if (team.speciality == '') {
    team.error = new Error("Especialidade não pode ficar em branco.");
  }
}

module.exports.all = function(fn) {
  daoConnector.textQuery(
    'SELECT t.*, teams.id, teams.speciality, mechanic1.name, mechanic2.name as name2\
     FROM teamMechanic AS t JOIN teams ON (teams.id = t.teamId)\
     JOIN staffs AS mechanic1 ON (mechanic1.id = t.mechanic1Id)\
     JOIN staffs as mechanic2 ON (mechanic2.id = t.mechanic2Id) ORDER BY teams.id;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allBySOId = function(soId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT\
      teams.id,\
      teams.speciality,\
      mechanicsWithTeamId.mechanic1Id,\
      mechanic1Name,\
      mechanicsWithTeamId.mechanic2Id,\
      mechanic2Name\
      FROM (SELECT teamMechanic.*,\
              mechanic1.name AS mechanic1Name,\
              mechanic2.name AS mechanic2Name\
              FROM teamMechanic\
              LEFT JOIN staffs AS mechanic1\
              ON (mechanic1.id = teamMechanic.mechanic1Id)\
              LEFT JOIN staffs AS mechanic2\
              ON (mechanic2.id = teamMechanic.mechanic2Id)) AS mechanicsWithTeamId\
      LEFT JOIN teams\
      ON (teams.id = mechanicsWithTeamId.teamId)\
      WHERE teams.id IN (SELECT teamId\
                          FROM teamsSO\
                          WHERE soId = $1) ORDER BY teams.id;',
    [soId],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.selectAllByIds = function(idsList, fn) {
  if (!idsList) return fn(null, []);
  var ids = (typeof idsList === 'string') ? [idsList] : idsList;
  ids = ids.map(function(id) { return Number(id); });

  daoConnector.paramaterizedQuery(
    'SELECT\
      teams.id,\
      teams.speciality,\
      mechanicsWithTeamId.mechanic1Id,\
      mechanic1Name,\
      mechanicsWithTeamId.mechanic2Id,\
      mechanic2Name\
      FROM (SELECT teamMechanic.*,\
              mechanic1.name AS mechanic1Name,\
              mechanic2.name AS mechanic2Name\
              FROM teamMechanic\
              LEFT JOIN staffs AS mechanic1\
              ON (mechanic1.id = teamMechanic.mechanic1Id)\
              LEFT JOIN staffs AS mechanic2\
              ON (mechanic2.id = teamMechanic.mechanic2Id)) AS mechanicsWithTeamId\
      LEFT JOIN teams\
      ON (teams.id = mechanicsWithTeamId.teamId)\
      WHERE ARRAY[teams.id] && $1;', [ids],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.allMechanics = function(fn) {
  daoConnector.textQuery("SELECT * FROM staffs WHERE role='Mecânico';", function(err, result) {
    if (err) {
      fn(err);
    } else {
      fn(null, result.rows);
    }
  });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT t.*, teams.id, teams.speciality, mechanic1.name, mechanic2.name as name2,\
      mechanic1.id AS mechanic1Id, mechanic2.id AS mechanic2Id\
      FROM teamMechanic AS t JOIN teams ON (teams.id = t.teamId)\
      JOIN staffs AS mechanic1 ON (mechanic1.id = t.mechanic1Id)\
      JOIN staffs as mechanic2 ON (mechanic2.id = t.mechanic2Id)\
      WHERE teamId=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM teams WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.selectByListOfIds = function(idList, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT * FROM teams\
      where ARRAY[id] && ($1);',
    [idList], function(err, result) {
      if (err) return fn(err);
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    });
};

module.exports.insertTeamWithMechanics = function (team, fn) {
  validateRequiredFields(team);
  if (team.error) {
    return fn(team.error);
  }
  if (team.mechanic1Id == team.mechanic2Id) {
    return fn(new Error('Mecânico 1 não pode ser igual ao mecânico 2!'), null);
  }
  daoConnector.transaction(function (err, dbClient, done) {
    dbClient.query('INSERT INTO teams (speciality) values ($1) RETURNING *',
      [team.speciality], function (err, result) {
        if (err) {
          done(err, fn);
        }
        else {
          team.id = result.rows[0].id;

          dbClient.query('INSERT INTO teamMechanic (teamId,\
            mechanic1Id, mechanic2Id) values ($1, $2, $3)',
            [team.id, team.mechanic1Id, team.mechanic2Id],
            function (err, result) {
              if (err) {
                done(err, fn);
              }
              else {
                done(null, function(err) {
                  fn(null, team);
                });
              }
          });
        }
      }
  );
  });
};

module.exports.updateTeamWithMechanics = function (team, fn) {
  validateRequiredFields(team);
  if (team.error) {
    return fn(team.error);
  }
  if (team.mechanic1Id == team.mechanic2Id) {
    return fn(new Error('Mecânico 1 não pode ser igual ao mecânico 2!'), team);
  }
  daoConnector.transaction(function (err, dbClient, done) {
    dbClient.query('UPDATE teams SET speciality=$2 WHERE id=$1', [team.id, team.speciality], function 
    (err, result) {
      if (err) {
        done(err, fn);
      }
      else {
        dbClient.query('UPDATE teamMechanic SET mechanic1Id=$2, mechanic2Id=$3\
        WHERE teamId = $1;', [team.id, team.mechanic1Id, team.mechanic2Id],
        function (err, result) {
              if (err) {
                done(err, fn);
              }
              else {
                done(null, function(err) {
                  fn(null, team);
                });
              }
          });
      }
    })
  });
};

module.exports.teamSuggestion = function(fn) {
  daoConnector.textQuery(
    "SELECT teams.*\
      FROM teams\
      LEFT JOIN teamsSo\
      ON (teamsSo.teamId = teams.id)\
      GROUP BY teams.id\
      ORDER BY COUNT(soid);", function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows[0]);
    }
  });
};

