var pg = require('pg');

var dbUrl = process.env.DATABASE_URL;

module.exports.textQuery = function(queryString, fn) {
  pg.connect(dbUrl, function(err, client, done) {
    if(err) {
      console.error('Could not connect to postgres', err);
      fn(err);
    }
    client.query(queryString, function() {
      done();
      fn.apply(null, arguments);
    });
  });
};

module.exports.paramaterizedQuery = function(queryString, queryParams, fn) {
  pg.connect(dbUrl, function(err, client, done) {
    if(err) {
      console.error('Could not connect to postgres', err);
      fn(err);
    }
    client.query(queryString, queryParams, function() {
      done();
      fn.apply(null, arguments);
    });
  });
};

module.exports.preparedQuery = function(statement) {
  pg.connect(dbUrl, function(err, client, done) {
    if(err) {
      console.error('Could not connect to postgres', err);
      fn(err);
    }
    client.query(statement, function() {
      done();
      fn.apply(null, arguments);
    });
  });
};

module.exports.transaction = function(fn) {
  pg.connect(dbUrl, function(err, client, done) {
    if(err) {
      console.error('Could not connect to postgres', err);
      fn(err);
    }

    client.query('BEGIN', function (err) {
      if (err) {
        end(err);
        fn(err);
      }
      fn.call(null, null, client, end);
    });

    function end(receivedError, callbackFn) {
      client.query(receivedError ? 'ROLLBACK' : 'COMMIT', function(err, result) {
        done(err);
        if (callbackFn instanceof Function) return callbackFn(receivedError || err, result);
      });
    };
  });
};