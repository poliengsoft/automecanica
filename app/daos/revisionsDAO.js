var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Revision = require('../model/revision');

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);
  rowFixer.inheritFrom(Revision);
}

module.exports.all = function(fn) {
  daoConnector.textQuery('\
    SELECT * FROM revisions\
    ', function (err, result) {
      if (err) {
        return fn(err);
      } else {
        var revisions = result.rows;
        fn(null, revisions);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('\
    SELECT * FROM revisions WHERE id = $1\
    ', [id], function (err, result) {
      if (err) {
        return fn(err);
      } else {
        var revision = result.rows[0];
        fn(null, revision);
      }
    });
};

module.exports.servicesIdsByRevisionId = function(revisionId, fn) {
  daoConnector.paramaterizedQuery('\
    SELECT serviceId\
    FROM revisionsservices\
    WHERE (revisionid = $1);\
    ', [revisionId], function (err, result) {
      if (err) {
        return fn(err);
      } else {
        fn(null, result.rows.map(function(row) {
          return row.serviceid;
        }));
      }
    });
};

module.exports.autoPartsIdsByRevisionId = function(revisionId, fn) {
  daoConnector.paramaterizedQuery('\
    SELECT autoPartId\
    FROM autopartsrevisions\
    WHERE (revisionid = $1);\
    ', [revisionId], function (err, result) {
      if (err) {
        return fn(err);
      } else {
        fn(null, result.rows.map(function(row) {
          return row.autopartid;
        }));
      }
    });
};

module.exports.insert = function (revision, fn) {
  validateRevision(revision);
  if (revision.error) {
    return fn(revision.error);
  }
  daoConnector.transaction(function (err, dbClient, done) {
    dbClient.query('INSERT INTO revisions\
      (name, description) VALUES ($1, $2) RETURNING *;\
      ', [revision.name, revision.description], function (err, result) {
        if (err) {
          done(err, fn);
        } else {
          revision.id = result.rows[0].id;
          revision.dbClient = dbClient;
          revision.done = done;
          revision.fn = fn;
          fillAutoPartsRevisionRelation(revision);
        }
      });
  });
};

function fillAutoPartsRevisionRelation (revision) {
  var query = buildAutoPartsRevisionsQuery(revision);
  if (query == '') {
    fillRevisionsServicesRelation(revision);
  } else {
    revision.dbClient.query(query, function (err, result) {
      if (err) {
        return revision.done(err, revision.fn);
      } else {
        fillRevisionsServicesRelation(revision);
      }
    });
  }
}

function fillRevisionsServicesRelation(revision) {
  var query = buildRevisionsServicesQuery(revision);
  revision.dbClient.query(query, function (err, result) {
    if (err) {
      return revision.done(err, revision.fn);
    }
    else {
      revision.done(null, revision.fn);
    }
  });
}

function buildAutoPartsRevisionsQuery(revision) {
  if (revision.autoParts.length == 0) return '';
  var query  = 'INSERT INTO autoPartsRevisions VALUES ';
  var queryValuesList = [];
  revision.autoParts.forEach(function (autoPart) {
    queryValuesList.push( buildQueryValues(revision.id, autoPart.id, 1) );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildRevisionsServicesQuery(revision) {
  var query  = 'INSERT INTO revisionsServices VALUES ';
  var queryValuesList = [];
  revision.services.forEach(function (service) {
    queryValuesList.push( buildQueryValues(revision.id, service.id) );
  });
  var queryValues = queryValuesList.join(', ');

  return query + queryValues;
}

function buildQueryValues() {
  return '(' + Array.prototype.join.call(arguments, ', ') + ')';
}

function validateRevision(revision) {
  if (revision.services.length == 0) {
    revision.error = new Error("É obrigatório escolher ao menos um serviço.");
  } else if (revision.name == '') {
    revision.error = new Error("É obrigatório dar um nome à revisão.");
  } else if (revision.description == '') {
    revision.error = new Error("É obrigatório dar descrição à revisão.");
  }
};
