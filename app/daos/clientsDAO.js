var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Client = require('../model/client');

function validateRequiredFields(client) {
  if (client.name == '' ||
      client.email == '' ||
        client.address == '' ||
          client.password == '' ||
            client.telephone1 == '') {
    client.error = new Error("Campo obrigatório vazio!");
  }
}

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT * FROM clients;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(function(row) {
        row.__proto__ = Client.prototype;
      });
      fn(null, result.rows);
    }
  });
};

module.exports.insert = function(client, fn) {
  validateRequiredFields(client);
  if (client.error){
    return fn(client.error);
  }
  daoConnector.paramaterizedQuery(
    'INSERT INTO clients' + 
    ' (name, email, address, password, telephone1, telephone2)' +
    ' VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;', 
    [
      client.name,
      client.email,
      client.address,
      client.password,
      client.telephone1,
      client.telephone2
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Client.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM clients WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Client.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectByEmail = function(email, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM clients WHERE email=$1;', [email],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(function(row) {
          row.__proto__ = Client.prototype;
        });
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(client, fn) {
  validateRequiredFields(client);
  if (client.error){
    return fn(client.error);
  }
  daoConnector.paramaterizedQuery(
    'UPDATE clients SET\
      name=$2, email=$3, address=$4,\
      telephone1=$5, telephone2=$6 WHERE id=$1;',
    [
      client.id,
      client.name,
      client.email,
      client.address,
      client.telephone1,
      client.telephone2
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        client.__proto__ = Client.prototype;
        fn(null, client);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM clients WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.updatePassword = function(client, fn) {
  daoConnector.paramaterizedQuery(
    'UPDATE clients SET\
      password=$2 WHERE id=$1;',
    [
      client.id,
      client.password
    ],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        client.__proto__ = Client.prototype;
        fn(null, client);
      }
    });
};
