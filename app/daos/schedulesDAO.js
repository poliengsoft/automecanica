var daoConnector = require('./daoConnector');
var ObjectFixer = require('../services/objectFixer');
var Schedule = require('../model/schedule');
var Client = require('../model/client');

var moment = require('moment');

function standardizeRow(row) {
  var rowFixer = new ObjectFixer(row);

  rowFixer.inheritFrom(Schedule);

  rowFixer.fix('datetime', function(datetime) {
    return new Date(row.datetime);
  });
  
  rowFixer.rename('vehicleid', 'vehicleId');
  rowFixer.rename('clientname', 'clientName');
  rowFixer.rename('licenseplate', 'licensePlate');
  rowFixer.rename('clientid', 'clientId');
};

module.exports.all = function(fn) {
  daoConnector.textQuery('SELECT * FROM schedules;', function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allWithClientName = function(fn) {
  daoConnector.textQuery(
    'SELECT\
      schedules_vehicles.id, schedules_vehicles.datetime,\
      schedules_vehicles.vehicleId, clients.name AS clientName,\
      schedules_vehicles.licensePlate, clients.id AS clientId\
      FROM (SELECT schedules.*, vehicles.clientId, vehicles.licensePlate\
              FROM schedules LEFT JOIN vehicles\
              ON schedules.vehicleId = vehicles.id) AS schedules_vehicles\
      LEFT JOIN clients\
      ON schedules_vehicles.clientId = clients.id;',
    function(err, result) {
    if (err) {
      fn(err);
    } else {
      result.rows.forEach(standardizeRow);
      fn(null, result.rows);
    }
  });
};

module.exports.allByClientId = function(clientId, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT\
      schedules_vehicles.id, schedules_vehicles.datetime,\
      schedules_vehicles.vehicleId, clients.name AS clientName,\
      schedules_vehicles.licensePlate, clients.id AS clientId\
      FROM (SELECT schedules.*, vehicles.clientId, vehicles.licensePlate\
              FROM schedules LEFT JOIN vehicles\
              ON schedules.vehicleId = vehicles.id) AS schedules_vehicles\
      LEFT JOIN clients\
      ON schedules_vehicles.clientId = clients.id\
      WHERE schedules_vehicles.clientId = $1;',
    [clientId],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
  });
};

module.exports.insert = function(schedule, fn) {
  daoConnector.paramaterizedQuery(
    'INSERT INTO schedules (datetime, vehicleId)\
      VALUES ($1, $2)\
      RETURNING *;',
    [schedule.datetime, schedule.vehicleId],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectById = function(id, fn) {
  daoConnector.paramaterizedQuery('SELECT * FROM schedules WHERE id=$1;', [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.selectByDate = function(date, fn) {
  var dateOfYear = moment(date).format('YYYY-MM-DD');
  daoConnector.paramaterizedQuery(
    "SELECT * FROM schedules\
      WHERE EXTRACT(DOY FROM datetime) = EXTRACT(DOY FROM DATE($1));",
    [dateOfYear],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows);
      }
    });
};

module.exports.selectWithClientNameById = function(id, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT\
      schedules_vehicles.id, schedules_vehicles.datetime,\
      schedules_vehicles.vehicleId, clients.name AS clientName,\
      schedules_vehicles.licensePlate, clients.id AS clientId\
      FROM (SELECT schedules.*, vehicles.clientId, vehicles.licensePlate\
              FROM schedules LEFT JOIN vehicles\
              ON schedules.vehicleId = vehicles.id) AS schedules_vehicles\
      LEFT JOIN clients\
      ON schedules_vehicles.clientId = clients.id\
      WHERE schedules_vehicles.id=$1;',
        [id],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        result.rows.forEach(standardizeRow);
        fn(null, result.rows[0]);
      }
    });
};

module.exports.update = function(schedule, fn) {
  daoConnector.paramaterizedQuery(
    'UPDATE schedules SET datetime=$2 WHERE id=$1;',
    [schedule.id, schedule.datetime],
    function(err, result) {
      if (err) {
        fn(err);
      } else {
        schedule.__proto__ = Schedule.prototype;
        fn(null, schedule);
      }
    });
};

module.exports.deleteById = function(id, fn) {
  daoConnector.paramaterizedQuery('DELETE FROM schedules WHERE id=$1;', [id],
    function(err, result) {
      fn(err);
    });
};

module.exports.countAllBetween = function(startDate, endDate, fn) {
  daoConnector.paramaterizedQuery(
    'SELECT COUNT(*)\
      FROM schedules\
      WHERE datetime > $1 AND datetime < $2;',
      [startDate, endDate],
      function(err, result) {
        if (err) return fn(err);
        fn(null, result.rows[0].count);
      });
};