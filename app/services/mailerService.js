require('dotenv').load();
var nodeMailer = require("nodemailer");
var jade = require("jade");


var transporterOptions = {
  service: process.env.EMAIL_SERVICE,
  auth: {
    user: process.env.EMAIL_LOGIN,
    pass: process.env.EMAIL_PASSWORD
  }
};

var transporter = nodeMailer.createTransport(transporterOptions, function(err, r) {
  if (err) {
    console.log("erro");
    throw (err)
  }
});

var jadeOptions = {
  filename: 'app/views/render.js',
  pretty: true,
  compileDebug: true
};

function send(emailType, context) {
  var emailPath = './app/views/mailer/' + emailType + '.jade'; // Path relative to the root init.js

  var htmlCompiler = jade.compileFile(emailPath, jadeOptions);

  var htmlContent = htmlCompiler(context);

  if (process.env.ENV === 'PRODUCTION') {
    transporter.sendMail({
      from: process.env.EMAIL,
      to: context.destinationEmail,
      subject: context.emailSubject,
      html: htmlContent
    }, function(err, info) {
      if (err) {
        console.log("Erro:");
        throw (err);
      }
    });
  } else {
    console.log("Email sent:");
    console.log(htmlContent);
  }
};

module.exports.sendWelcomeToStaff = function(staff) {
  var context = {};
  context.destinationEmail = staff.email;
  context.password = staff.password;
  context.emailSubject = "Welcome " + staff.name;
  context.user = staff;
  send('welcomeStaff', context);
}

module.exports.sendWelcomeToClient = function(client) {
  var context = {};
  context.destinationEmail = client.email;
  context.password = client.password;
  context.emailSubject = "Welcome " + client.name;
  context.user = client;
  send('welcomeClient', context);
}
