var bcrypt = require('bcrypt');
var randToken = require('rand-token');

var PASSWORD_LENGTH = 8;

module.exports.hashPassword = function(password, fn) {
  bcrypt.genSalt(8, function(err, salt) {
    bcrypt.hash(password, salt, function(err, hash) {
      fn(err, hash);
    });
  });
};

module.exports.generatePassword = function(fn) {
  var password = randToken.generate(PASSWORD_LENGTH);
  bcrypt.genSalt(8, function(err, salt) {
    bcrypt.hash(password, salt, function(err, hash) {
      fn(err, password, hash);
    });
  });
};

module.exports.checkPassword = function(password, hash, fn) {
  bcrypt.compare(password, hash, function(err, res) {
    fn(err, res);
  });
};