module.exports = (function() {
  function ObjectFixer(obj) {
    this.obj = obj;
  };

  ObjectFixer.prototype.inheritFrom = function(baseObject) {
    this.obj.__proto__ = baseObject.prototype;
  };

  ObjectFixer.prototype.fix = function(attrName, fixer) {
    if (typeof this.obj[attrName] !== 'undefined') {
      if (typeof fixer === 'function') {
        this.obj[attrName] = fixer(this.obj[attrName]);
      }
      else {
        this.obj[attrName] = fixer;
      }
    };
  };

  ObjectFixer.prototype.rename = function(attrName, newAttrName) {
    if (typeof this.obj[attrName] !== 'undefined') {
      this.obj[newAttrName] = this.obj[attrName];
      delete this.obj[attrName];
    };
  };

  return ObjectFixer;
})();