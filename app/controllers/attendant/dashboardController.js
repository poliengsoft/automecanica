var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('attendant/dashboard/index');
});

module.exports = router;
