var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');

var passwordManager = rootRequire('app/services/passwordManager');

router.get('/password', function (req, res, next) {
  res.locals.attendant = {};
  res.render('attendant/attendant/password');
});

router.patch('/password', 
  validatePasswordConfirmation,
  validateCurrentPassword,
  updateCurrentPassword);

function validatePasswordConfirmation(req, res, next) {
  var form =  req.body;

  if (form.newPassword == form.newPasswordConfirmation 
      && form.newPassword != '') return next();
  else {
    req.flash('error', 'A senha nova e a confirmações estão diferentes ou vazias');
    res.redirect('/attendant-panel/password');
  }
};

function validateCurrentPassword(req, res, next) {
  var form =  req.body;
  var currentStaffPassword = req.currentStaff.password;

  passwordManager.checkPassword(
    form.currentPassword, currentStaffPassword,
    function (err, isPasswordValid) {
      if (err || !isPasswordValid) {
        req.flash('error', 'Senha Atual incorreta');
        res.redirect('/attendant-panel/password');
      }
      else return next();
    });
};

function updateCurrentPassword(req, res, next) {
  var attendant =  req.body;
  attendant.id = req.currentStaff.id;

  passwordManager.hashPassword(attendant.newPassword, function (err, hash) {
    attendant.password = hash;
    staffsDAO.updatePassword(attendant, function(err) {
      if (err) {
        req.flashUpdateError('Senha');
        res.redirect('/attendant-panel/password');
        next(err);
      } else {
        req.flashUpdateSuccess('Senha');
        res.redirect('/attendant-panel/attendant');
      }
    });
  });
};

module.exports = router;
