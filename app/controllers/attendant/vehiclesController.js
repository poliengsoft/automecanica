var express = require('express');
var router = express.Router();

var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Vehicle = rootRequire('app/model/vehicle');

router.get('/vehicles', function (req, res, next) {
  vehiclesDAO.all(function(err, vehicles) {
    res.locals.vehicles = vehicles;
    res.render('attendant/vehicles/index');
  });
});

router.get('/clients/:clientId/vehicles/new', function (req, res, next) {
  res.locals.vehicle = new Vehicle();
  res.locals.vehicle.clientId = req.params.clientId;
  res.render('attendant/vehicles/new');
});

router.get('/vehicles/:id', function (req, res, next) {
  vehiclesDAO.selectById(req.params.id, function(err, vehicle) {
    if (err) {
      res.redirect('/attendant-panel/clients');
    } else {
      res.locals.vehicle = vehicle;
      res.render('attendant/vehicles/show');
    }
  });
});

router.post('/clients/:clientId/vehicles', function (req, res, next) {
  var vehicleForm = new Vehicle(req.body);
  vehicleForm.clientId = req.params.clientId;

  vehiclesDAO.insert(vehicleForm, function(err, vehicle) {
    if (err) {
      req.flashCreateError('Veículo');
      res.locals.vehicle = vehicleForm;
      res.render('attendant/vehicles/new');
      next(err);
    } else {
      req.flashCreateSuccess('Veículo');
      res.redirect('/attendant-panel/vehicles/' + vehicle.id);
    }
  });
});

router.get('/vehicles/:id/edit', function (req, res, next) {
  vehiclesDAO.selectById(req.params.id, function(err, vehicle) {
    if (err) {
      res.redirect('/attendant-panel/vehicles');
      next(err);
    } else {
      res.locals.vehicle = vehicle;
      res.render('attendant/vehicles/edit');
    }
  });
});

router.patch('/vehicles/:id', function (req, res, next) {
  var vehicle =  new Vehicle(req.body);
  vehicle.id = req.params.id;

  vehiclesDAO.update(vehicle, function(err) {
    if (err) {
      req.flashUpdateError('Veículo');
      res.locals.vehicle = vehicle;
      res.render('attendant/vehicles/edit');
      next(err);
    } else {
      req.flashUpdateSuccess('Veículo');
      res.redirect('/attendant-panel/vehicles/'+ vehicle.id);
    }
  });
});

router.delete('/vehicles/:id', function (req, res, next) {
  var clientId = req.body._clientId;
  vehiclesDAO.deleteById(req.params.id, function(err) {
    res.redirect('/attendant-panel/clients/' + clientId);
  });
});

module.exports = router;
