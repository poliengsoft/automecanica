var express = require('express');
var router = express.Router();

var sosDAO = rootRequire('app/daos/sosDAO');

router.get('/sos', function (req, res, next) {
  sosDAO.allWithCarPlate(function(err, sos){
    res.locals.sos = sos;
    res.render('attendant/sos/index');
  })
});

router.post('/sos/:id', function (req, res, next) {
  sosDAO.updatePaymentStatus(req.params.id, function(err, wasUpdate) {
    if (err || !wasUpdate) {
      req.flashPaymentError();
    }
    else {
      req.flashPaymentSuccess();
    }
    res.redirect('/attendant-panel/sos');
  });
});

module.exports = router;
