var express = require('express');
var router = express.Router();

var schedulesDAO = rootRequire('app/daos/schedulesDAO');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Schedule = rootRequire('app/model/schedule');

var moment = require('moment');

router.get('/schedules', function (req, res, next) {
  schedulesDAO.allWithClientName(function(err, schedules) {
    res.locals.schedules = schedules;
    res.render('attendant/schedules/index');
  });
});

router.get('/schedules/new', function (req, res, next) {
  vehiclesDAO.all(function(err, vehicles) {
    if (err) {
      res.redirect('/attendant-panel/schedules');
      next(err);
    } else {
      res.locals.schedule = new Schedule();
      res.locals.vehicles = vehicles;
      res.render('attendant/schedules/new');
    }
  });
});

router.get('/schedules/:id', function (req, res, next) {
  schedulesDAO.selectWithClientNameById(req.params.id, function(err, schedule) {
    if (err) {
      res.redirect('/attendant-panel/schedules');
      next(err);
    } else {
      res.locals.schedule = schedule;
      res.render('attendant/schedules/show');
    }
  });
});

router.post('/schedules', function (req, res, next) {
  var scheduleForm = new Schedule(req.body);

  schedulesDAO.insert(scheduleForm, function(err, schedule) {
    if (err) {
      res.locals.schedule = scheduleForm;
      req.flashCreateError('Agendamento');
      vehiclesDAO.all(function(err, vehicles) {
        res.locals.vehicles = vehicles;
        res.render('attendant/schedules/new');
        next(err);
      });
    } else {
      req.flashCreateSuccess('Agendamento');
      res.redirect('/attendant-panel/schedules');
    }
  });
});

router.get('/schedules/:id/edit', function (req, res, next) {
  schedulesDAO.selectById(req.params.id, function(err, schedule) {
    if (err) {
      res.redirect('/attendant-panel/schedules');
      next(err);
    } else {
      res.locals.schedule = schedule;
      res.render('attendant/schedules/edit');
    }
  });
});

router.patch('/schedules/:id', function (req, res, next) {
  var schedule =  new Schedule(req.body);
  schedule.id = req.params.id;

  schedulesDAO.update(schedule, function(err) {
    if (err) {
      req.flashUpdateError('Agendamento');
      res.locals.schedule = schedule;
      res.render('attendant/schedules/edit');
      next(err);
    } else {
      req.flashUpdateSuccess('Agendamento');
      res.redirect('/attendant-panel/schedules');
    }
  });
});

router.delete('/schedules/:id', function (req, res, next) {
  schedulesDAO.deleteById(req.params.id, function(err) {
    req.flashDeleteSuccess('Agendamento');
    res.redirect('/attendant-panel/schedules');
  });
});

module.exports = router;
