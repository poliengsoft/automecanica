var express = require('express');
var router = express.Router();

var clientsDAO = rootRequire('app/daos/clientsDAO');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Client = rootRequire('app/model/client');
var passwordManager = rootRequire('app/services/passwordManager');
var mailer = rootRequire('app/services/mailerService.js');

router.get('/clients', function (req, res, next) {
  clientsDAO.all(function(err, clients) {
    res.locals.clients = clients;
    res.render('attendant/clients/index');
  });
});

router.get('/clients/new', function (req, res, next) {
  res.locals.client = new Client();
  res.render('attendant/clients/new');
});

router.get('/clients/:id', function (req, res, next) {
  clientsDAO.selectById(req.params.id, function(err, client) {
    if (err) {
      res.redirect('/attendant-panel/clients');
    } else {
      res.locals.client = client;
      vehiclesDAO.allByClientId(client.id, function(err, vehicles) {
        if (err) {
          next(err);
        }
        res.locals.vehicles = vehicles;
        res.render('attendant/clients/show');
      });
    }
  });
});

router.post('/clients', function (req, res, next) {
  var clientForm = new Client(req.body);

  passwordManager.generatePassword(function(err, password, hash) {
    clientForm.password = hash;
    clientsDAO.insert(clientForm, function(err, client) {
      if (err) {
        delete clientForm.password;
        res.locals.client = clientForm;
        res.render('attendant/clients/new');
        next(err);
      } else {
        client.password = password;
        mailer.sendWelcomeToClient(client);
        res.redirect('/attendant-panel/clients');
      }
    });
  });
});

router.get('/clients/:id/edit', function (req, res, next) {
  clientsDAO.selectById(req.params.id, function(err, client) {
    if (err) {
      res.redirect('/attendant-panel/clients');
      next(err);
    } else {
      res.locals.client = client;
      res.render('attendant/clients/edit');
    }
  });
});

router.patch('/clients/:id', function (req, res, next) {
  var client =  new Client(req.body);
  client.id = req.params.id;

  clientsDAO.update(client, function(err) {
    if (err) {
      res.locals.client = client;
      res.render('attendant/clients/edit');
      next(err);
    } else {
      res.redirect('/attendant-panel/clients');
    }
  });
});

router.delete('/clients/:id', function (req, res, next) {
  clientsDAO.deleteById(req.params.id, function(err) {
    res.redirect('/attendant-panel/clients');
  });
});

module.exports = router;
