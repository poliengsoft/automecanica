var express = require('express');
var router = express.Router();

var schedulesDAO = rootRequire('app/daos/schedulesDAO');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Schedule = rootRequire('app/model/schedule');

var moment = require('moment');

router.get('/schedules/free-hours-by-date',
  getSchedulesFromDate,
  createUsedHours,
  returnFreeHours
);

function getSchedulesFromDate(req, res, next) {
  var date = new Date(req.query.date);
  schedulesDAO.selectByDate(date, function(err, schedules) {
    if (err) return next(err);
    res.locals.schedules = schedules;
    next();
  });
}

function createUsedHours(req, res, next) {
  var schedules = res.locals.schedules;
  res.locals.usedHours = schedules.map(function(schedule) {
    return moment(schedule.datetime).format('HH:mm');
  });
  next();
}

function returnFreeHours(req, res, next) {
  var usedHours = res.locals.usedHours;
  var freeHours = Schedule.WORK_HOURS.filter(byUsedHours);
  res.json(freeHours);

  function byUsedHours(hour) {
    return (usedHours.indexOf(hour) < 0);
  }
}

module.exports = router;