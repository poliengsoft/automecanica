var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');

var passwordManager = rootRequire('app/services/passwordManager');

router.get('/password', function (req, res, next) {
  res.locals.technician = {};
  res.render('technician/technician/password');
});

router.patch('/password', 
  validatePasswordConfirmation,
  validateCurrentPassword,
  updateCurrentPassword);

function validatePasswordConfirmation(req, res, next) {
  var form =  req.body;

  if (form.newPassword == form.newPasswordConfirmation 
      && form.newPassword != '') return next();
  else {
    req.flash('error', 'A senha nova e a confirmações estão diferentes ou vazias');
    res.redirect('/technician-panel/password');
  }
};

function validateCurrentPassword(req, res, next) {
  var form =  req.body;
  var currentStaffPassword = req.currentStaff.password;

  passwordManager.checkPassword(
    form.currentPassword, currentStaffPassword,
    function (err, isPasswordValid) {
      if (err || !isPasswordValid) {
        req.flash('error', 'Senha Atual incorreta');
        res.redirect('/technician-panel/password');
      }
      else return next();
    });
};

function updateCurrentPassword(req, res, next) {
  var technician =  req.body;
  technician.id = req.currentStaff.id;

  passwordManager.hashPassword(technician.newPassword, function (err, hash) {
    technician.password = hash;
    staffsDAO.updatePassword(technician, function(err) {
      if (err) {
        req.flashUpdateError('Houve uma falha para salvar');
        res.redirect('/technician-panel/password');
        next(err);
      } else {
        req.flashUpdateSuccess('Senha');
        res.redirect('/technician-panel/technician');
      }
    });
  });
};

module.exports = router;
