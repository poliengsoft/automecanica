var moment = require('moment');

var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var clientsDAO = rootRequire('app/daos/clientsDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

module.exports = function (router) {
  router.post('/sos/preview',
    preparePreview,
    getVehicle,
    getVehicleClient,
    getTeams,
    revisionHandler,
    getServices,
    getAutoParts,
    calculateTotalPrice,
    renderSOPreview
  );
};

function preparePreview(req, res, next) {
  res.locals.dueDate = moment(req.body.dueDate);
  next()
};

function getVehicle(req, res, next) {
  var vehicleId = req.body.vehicleId;
  vehiclesDAO.selectById(vehicleId, function(err, vehicle) {
    if (err) return next(err);
    res.locals.vehicle = vehicle;
    next();
  });
};

function getVehicleClient(req, res, next) {
  var clientId = res.locals.vehicle.clientId;
  clientsDAO.selectById(clientId, function(err, client) {
    if (err) return next(err);
    res.locals.client = client;
    next();
  });
};

function getTeams(req, res, next) {
  var teamsIds = req.body['teamsIds[]'];
  teamsDAO.selectAllByIds(teamsIds, function(err, teams) {
    if (err) return next(err);
    res.locals.teams = teams;
    next();
  });
};

function revisionHandler(req, res, next) {
  var revisionId = req.body.revisionId;
  if (revisionId) {
    revisionsDAO.servicesIdsByRevisionId(revisionId, function(err, servicesIds) {
      if (err) return next(err);
      req.body['servicesIds[]'] = servicesIds;
      revisionsDAO.autoPartsIdsByRevisionId(revisionId, function(err, autoPartsIds) {
        if (err) return next(err);
        req.body['autoPartsIds[]'] = autoPartsIds;
        next();
      });
    });
  }
  else {
    next();
  }
};

function getServices(req, res, next) {
  var servicesIds = req.body['servicesIds[]'];
  servicesDAO.selectAllByIds(servicesIds, function(err, services) {
    if (err) return next(err);
    res.locals.services = services;
    next();
  });
};

function getAutoParts(req, res, next) {
  var autoPartsIds = req.body['autoPartsIds[]'];
  autoPartsDAO.selectAllByIds(autoPartsIds, function(err, autoParts) {
    if (err) return next(err);
    res.locals.autoParts = autoParts;
    next();
  });
};

function calculateTotalPrice(req, res, next) {
  res.locals.totalPrice = 0;
  var services = res.locals.services;
  var autoParts = res.locals.autoParts;
  if (services) {
    services.forEach(function(service) {
      res.locals.totalPrice += parseFloat(service.price);
    });
  }
  if (autoParts) {
    autoParts.forEach(function(autoPart) {
      res.locals.totalPrice += parseFloat(autoPart.price);
    });
  }
  next();
};

function renderSOPreview(req, res, next) {
  res.render('technician/sos/preview');
};