var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var SO = rootRequire('app/model/so');

module.exports = function (router) {
  router.get('/sos/newFromRevision',
    getVehicles,
    getTeams,
    getRevisions,
    getTeamSuggestion,
    renderSO
  );
};


function getVehicles(req, res, next) {
  vehiclesDAO.all(function(err, vehicles) {
    if (err) {
      req.flashNotFound('veículos'); 
      return next(err);
    }
    res.locals.vehicles = vehicles;
    next();
  });
};

function getTeams(req, res, next) {
  teamsDAO.all(function(err, teams) {
    if (err) {
      req.flashNotFound('equipes'); 
      return next(err);
    }
    res.locals.teams = teams;
    next();
  });
};

function getRevisions(req, res, next) {
  revisionsDAO.all(function(err, revisions) {
    if (err) {
      req.flashNotFound('revisões'); 
      return next(err);
    }
    res.locals.revisions = revisions;
    next();
  });
};

function getTeamSuggestion(req, res, next) {
  teamsDAO.teamSuggestion(function(err, team) {
    if (err) {
      req.flashNotFound('equipe Sugerida'); 
      return next(err);
    }
    res.locals.teamSuggestion = team;
    next();
  });
};

function renderSO(req, res, next) {
  res.locals.so = new SO();
  res.render('technician/sos/newFromRevision');
};