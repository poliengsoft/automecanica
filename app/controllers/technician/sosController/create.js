var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var sosDAO = rootRequire('app/daos/sosDAO');
var SO = rootRequire('app/model/so');

module.exports = function (router) {
  router.post('/sos',
    prepareSO,
    getSOVehicles,
    getSOTeams,
    revisionHandler,
    getSOServices,
    getSOAutoParts,
    calculateSOPrice,
    createSO
  );
};

function prepareSO(req, res, next) {
  so = new SO(req.body);
  so.price = 0;
  res.locals.so = so;
  next();
}

function getSOVehicles(req, res, next) {
  var vehicleId = req.body.vehicleId;
  res.locals.so.vehicleId = vehicleId;
  next();
};

function getSOTeams(req, res, next) {
  var teamsIds = req.body.teamsIds;
  teamsIds = convertToArray(teamsIds);
  teamsDAO.selectByListOfIds(teamsIds, function (err, result) {
    if (err) {
      return next(err);
    }
    res.locals.so.teams = result;
    next();
  });
};

function revisionHandler(req, res, next) {
  var revisionId = req.body.revisionId;
  if (revisionId) {
    revisionsDAO.servicesIdsByRevisionId(revisionId, function(err, servicesIds) {
      if (err) return next(err);
      req.body.servicesIds = servicesIds;
      revisionsDAO.autoPartsIdsByRevisionId(revisionId, function(err, autoPartsIds) {
        if (err) return next(err);
        req.body.autoPartsIds = autoPartsIds;
        next();
      });
    });
  }
  else {
    next();
  }
};

function getSOServices(req, res, next) {
  var servicesIds = req.body.servicesIds;
  servicesIds = convertToArray(servicesIds);
  servicesDAO.selectByListOfIds(servicesIds, function (err, result) {
    if (err) {
      return next(err);
    }
      res.locals.so.tasks = result; // Here services changes to tasks
    next();
  });
};

function getSOAutoParts(req, res, next) {
  var autoPartsIds = req.body.autoPartsIds;
  autoPartsIds = convertToArray(autoPartsIds);
  autoPartsDAO.selectByListOfIds(autoPartsIds, function (err, result) {
    if (err) {
      return next(err);
    }
    res.locals.so.autoParts = result;
    next();
  });
};

function calculateSOPrice(req, res, next) {
  var tasks = res.locals.so.tasks;
  if (tasks) {
    tasks.forEach(function(task) {
      res.locals.so.price += parseFloat(task.price);
    });
  }
  var autoParts = res.locals.so.autoParts;
  if (autoParts) {
    autoParts.forEach(function(autoPart) {
      autoPart.quantity = 1;
      res.locals.so.price += parseFloat(autoPart.price) * parseInt(autoPart.quantity);
    });
  }
  next();
};

function createSO(req, res, next) {
  sosDAO.insert(res.locals.so, function (err, result) {
    if (err) {
      delete res.locals.so.id;
      req.flashCreateError('Ordem de serviço');
      res.redirect('/technician-panel/sos/new');
    }
    else {
      req.flashCreateSuccess('Ordem de serviço');
      res.redirect('/technician-panel/sos');
    }
  });
};

function convertToArray(obj) {
  return (obj instanceof Array) ? obj : [obj];
};
