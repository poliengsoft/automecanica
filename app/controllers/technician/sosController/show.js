var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var clientsDAO = rootRequire('app/daos/clientsDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

module.exports = function (router) {
  router.get('/sos/:id',
    getSO,
    getSOVehicle,
    getVehicleClient,
    getSOTeams,
    getSOServices,
    getSOAutoParts,
    renderSOShow
  );
};

function getSO(req, res, next) {
  sosDAO.selectById(req.params.id, function (err, so) {
    res.locals.so = so;
    next();
  });
}

function getSOVehicle(req, res, next) {
  var so = res.locals.so;
  vehiclesDAO.selectById(so.vehicleId, function (err, vehicle) {
    res.locals.vehicle = vehicle;
    next();
  });
}

function getVehicleClient(req, res, next) {
  var vehicle = res.locals.vehicle;
  clientsDAO.selectById(vehicle.clientId, function (err, client) {
    res.locals.client = client;
    next();
  });
}

function getSOTeams(req, res, next) {
  var so = res.locals.so;
  teamsDAO.allBySOId(so.id, function (err, teams) {
    res.locals.teams = teams;
    next();
  });
}

function getSOServices(req, res, next) {
  var so = res.locals.so;
  servicesDAO.allBySOId(so.id, function (err, services) {
    res.locals.services = services;
    next();
  });
}

function getSOAutoParts(req, res, next) {
  var so = res.locals.so;
  autoPartsDAO.allBySOId(so.id, function (err, autoParts) {
    res.locals.autoParts = autoParts;
    next();
  });
}

function renderSOShow(req, res, next) {
  res.render('technician/sos/show');
};