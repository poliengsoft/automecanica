var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var SO = rootRequire('app/model/so');

module.exports = function (router) {
  router.get('/sos/new',
    getVehicles,
    getTeams,
    getServices,
    getAutoParts,
    getTeamSuggestion,
    renderSO
  );
};


function getVehicles(req, res, next) {
  vehiclesDAO.all(function(err, vehicles) {
    if (err) {
      req.flashNotFound('veículos'); 
      return next(err);
    }
    res.locals.vehicles = vehicles;
    next();
  });
};

function getTeams(req, res, next) {
  teamsDAO.all(function(err, teams) {
    if (err) {
      req.flashNotFound('equipes'); 
      return next(err);
    }
    res.locals.teams = teams;
    next();
  });
};

function getAutoParts(req, res, next) {
  autoPartsDAO.all(function(err, autoParts) {
    if (err) {
      req.flashNotFound('peças'); 
      return next(err);
    }
    res.locals.autoParts = autoParts;
    next();
  });
};

function getServices(req, res, next) {
  servicesDAO.all(function(err, services) {
    if (err) {
      req.flashNotFound('serviços'); 
      return next(err);
    }
    res.locals.services = services;
    next();
  });
};

function getTeamSuggestion(req, res, next) {
  teamsDAO.teamSuggestion(function(err, team) {
    if (err) {
      req.flashNotFound('equipe Sugerida'); 
      return next(err);
    }
    res.locals.teamSuggestion = team;
    next();
  });
};

function renderSO(req, res, next) {
  res.locals.so = new SO();
  res.render('technician/sos/new');
};