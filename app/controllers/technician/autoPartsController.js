var express = require('express');
var router = express.Router();

var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var AutoPart = rootRequire('app/model/autoPart');

router.get('/autoParts', function (req, res, next) {
  autoPartsDAO.all(function(err, autoParts) {
    res.locals.autoParts = autoParts;
    res.render('technician/autoParts/index');
  });
});

router.get('/autoParts/:id', function (req, res, next) {
  autoPartsDAO.selectById(req.params.id, function(err, autoPart) {
    if (err) {
      res.redirect('/technician-panel/autoParts');
      next(err);
    } else {
      res.locals.autoPart = autoPart;
      res.render('technician/autoParts/show');
    }
  });
});

module.exports = router;
