var express = require('express');
var router = express.Router();

var clientsDAO = rootRequire('app/daos/clientsDAO');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Client = rootRequire('app/model/client');

router.get('/clients', function (req, res, next) {
  clientsDAO.all(function(err, clients){
    res.locals.clients = clients;
    res.render('technician/clients/index');
  })
});

router.get('/clients/:id', function (req, res, next) {
  clientsDAO.selectById(req.params.id, function(err, client) {
    if (err) {
      res.redirect('/technician-panel/clients');
    } else {
      res.locals.client = client;
      vehiclesDAO.allByClientId(client.id, function(err, vehicles) {
        if (err) {
          next(err);
        }
        res.locals.vehicles = vehicles;
        res.render('technician/clients/show');
      });
    }
  });
});

module.exports = router;