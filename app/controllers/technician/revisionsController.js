var express = require('express');
var router = express.Router();

var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var Revision = rootRequire('app/model/revision');

var setShowAction = require('./revisionsController/show');

router.get('/revisions', function (req, res, next) {
  revisionsDAO.all(function(err, revisions) {
    res.locals.revisions = revisions;
    res.render('technician/revisions/index');
  });
});

setShowAction(router);

module.exports = router;
