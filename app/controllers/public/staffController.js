var express = require('express');
var router = express.Router();

var BaseModel = rootRequire('app/model/__base__');
var staffsDAO = rootRequire('app/daos/staffsDAO');

var passport = require('passport');

router.use('/staff', checkCurrentStaff);

router.get('/staff', function (req, res, next) {
  res.locals.form = new BaseModel();
  res.render('public/staff/new');
});

router.post('/staff', function (req, res, next) {
  passport.authenticate('staff-local', function(err, staff, info) {
    if (err) return next(err);
    if (!staff) {
      req.flashLoginError();
      return res.redirect('/staff');
    }
    req.logIn(staff, function(err) {
      if (err) {
        req.flashLoginError();
        res.redirect('/staff');
        return next(err);
      }
      req.flashLoginSuccess();
      next();
    });
  })(req, res, next);
}, redirectToCurrentPanel);

function checkCurrentStaff(req, res, next) {
  if (req.user && req.user.isStaff) {
    return redirectToCurrentPanel(req, res, next);
  }
  else return next();
};

function redirectToCurrentPanel(req, res, next) {
  switch(req.user.role) {
    case 'Gerente':
      return res.redirect('/admin-panel');
      break;
    case 'Atendente':
      return res.redirect('/attendant-panel');
      break;
    case 'Técnico':
      return res.redirect('/technician-panel');
      break;
    case 'Mecânico':
      return res.redirect('/mechanic-panel');
      break;
  }
};

module.exports = router;