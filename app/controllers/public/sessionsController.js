var express = require('express');
var router = express.Router();

var BaseModel = rootRequire('app/model/__base__');
var clientsDAO = rootRequire('app/daos/clientsDAO');

var passport = require('passport');

router.use('/login', function (req, res, next) {
  if (req.user && req.user.isClient) {
    req.flashLoginSuccess();
    res.redirect('/client-panel');
  } else {
    next();
  }
});

router.get('/login', function (req, res, next) {
  if (req.user && req.user.isClient) {
    req.flashLoginSuccess();
    res.redirect('/client-panel');
  } else {
    res.locals.form = new BaseModel();
    res.render('public/sessions/new');
  }
});

router.post('/login', function (req, res, next) {
  passport.authenticate('client-local', function(err, client, info) {
    if (err) return next(err);
    if (!client) {
      req.flashLoginError();
      return res.redirect('/login');
    }
    req.logIn(client, function(err) {
      if (err) return next(err);
      req.flashLoginSuccess();
      return res.redirect('/client-panel');
    });
  })(req, res, next);
});

router.get('/logout', function (req, res, next) {
  req.session.destroy(function(err) {
    res.redirect('/');
  });
});

module.exports = router;
