var express = require('express');
var router = express.Router();

var clientsDAO = rootRequire('app/daos/clientsDAO');
var Client = rootRequire('app/model/client');
var passwordManager = rootRequire('app/services/passwordManager');
var mailer = rootRequire('app/services/mailerService.js');

router.use('/registration', function (req, res, next) {
  if (req.user && req.user.isClient) {
    res.redirect('/client-panel/client');
  } else {
    next();
  }
});

router.get('/registration', function (req, res, next) {
  res.locals.client = new Client();
  res.render('public/clients/new');
});

router.post('/registration', passwordConfirmationCheck,
  function (req, res, next) {
    var password = req.body.password;
    passwordManager.hashPassword(password, function(err, hashPassword) {
      var clientForm = new Client(req.body);
      clientForm.password = hashPassword;
      clientsDAO.insert(clientForm, function(err, client) {
        if (err) {
          req.flash('error', 'Campos obrigatórios vazios!');
          delete clientForm.password;
          res.locals.client = clientForm;
          res.render('public/clients/new');
        } else {
          client.password = password;
          mailer.sendWelcomeToClient(client);
          req.login(client, function(err) {
            if (err) return next(err);
            req.session.isClientNew = true;
            req.flashLoginSuccess();
            res.redirect('/client-panel');
          });
        }
      });
    });
});

function passwordConfirmationCheck(req, res, next) {
  if (req.body.password === req.body.passwordConfirmation) {
    next();
  } else {
    delete req.body.password;
    delete req.body.passwordConfirmation;
    req.flashDifferentPasswords();
    res.locals.client = new Client(req.body);
    res.render('public/clients/new');
  };
}


module.exports = router;