var express = require('express');
var router = express.Router();

var schedulesDAO = rootRequire('app/daos/schedulesDAO');
var clientsDAO = rootRequire('app/daos/clientsDAO');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Schedule = rootRequire('app/model/schedule');

router.get('/schedules', function (req, res, next) {
  schedulesDAO.allByClientId(req.currentClient.id, function(err, schedules) {
    res.locals.schedules = schedules;
    res.render('client/schedules/index');
  });
});

router.get('/schedules/new', 
  getVehicles,
  function (req, res, next) {
    res.locals.isClientNew = req.session.isClientNew;
    res.locals.schedule = new Schedule();
    res.render('client/schedules/new');
  });

router.get('/schedules/:id', validateScheduleOwnership,
  function (req, res, next) {
    schedulesDAO.selectById(req.params.id, function(err, schedule) {
      if (err) return next(err);

      if (schedule) {
        res.locals.schedule = schedule;
        res.render('client/schedules/show');
      } else {
        res.redirect('/client-panel/schedules');
      }
    });
});

router.post('/schedules', function (req, res, next) {
  var scheduleForm = new Schedule(req.body);
  scheduleForm.clientId = req.currentClient.id;

  schedulesDAO.insert(scheduleForm, function(err, schedule) {
    if (err) {
      res.locals.schedule = scheduleForm;
      res.locals.isClientNew = req.session.isClientNew;
      req.flashCreateError('Agendamento');
      vehiclesDAO.allByClientId(req.currentClient.id, function(err, vehicles) {
        res.locals.vehicles = vehicles;
        res.render('client/schedules/new');
        next(err);
      });
    } else {
      req.flashCreateSuccess('Agendamento');
      if (req.session.isClientNew) delete req.session.isClientNew;
      res.redirect('/client-panel/schedules/' + schedule.id);
    }
  });
});

router.get('/schedules/:id/edit', validateScheduleOwnership,
  getVehicles,
  function (req, res, next) {
    schedulesDAO.selectById(req.params.id, function(err, schedule) {
      if (err) return next(err);

      if (schedule) {
        res.locals.schedule = schedule;
        res.render('client/schedules/edit');
      } else {
        res.redirect('/client-panel/schedules');
      }
    });
});

router.patch('/schedules/:id', validateScheduleOwnership,
  function (req, res, next) {
    var schedule = new Schedule(req.body);
    schedule.id = req.params.id;
    
    schedulesDAO.update(schedule, function(err) {
      if (err) {
        req.flashUpdateError('Agendamento');
        res.locals.schedule = schedule;
        vehiclesDAO.allByClientId(req.currentClient.id, function(err, vehicles) {
          res.locals.vehicles = vehicles;
          res.render('client/schedules/edit');
          next(err);
        });
      } else {
        req.flashUpdateSuccess('Agendamento');
        res.redirect('/client-panel/schedules');
      }
    });
  });

router.delete('/schedules/:id', validateScheduleOwnership,
  function (req, res, next) {
    schedulesDAO.deleteById(req.params.id, function(err) {
      req.flashDeleteSuccess('Agendamento');
      res.redirect('/client-panel/schedules');
    });
});

function validateScheduleOwnership(req, res, next) {
  schedulesDAO.selectWithClientNameById(req.params.id, function (err, schedule) {
    if (err || !schedule) {
      return next(err || { message: 'Schedule does not exists' });
    }
    if (schedule.clientId == req.currentClient.id) {
      next();
    } else {
      res.redirect('/client-panel/schedules');
    }
  });
};

function getVehicles(req, res, next) {
  vehiclesDAO.allByClientId(req.currentClient.id, function(err, vehicles) {
    res.locals.vehicles = vehicles;
    next();
  });
};

module.exports = router;
