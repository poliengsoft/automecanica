var express = require('express');
var router = express.Router();

var sosDAO = rootRequire('app/daos/sosDAO');

var setShowAction = require('./sosController/show');

router.get('/sos', function(req, res, next) {
  sosDAO.allByClientId(req.currentClient.id, function(err, sos) {
    if (err) return next(err);
    res.locals.sos = sos;
    res.render('client/sos/index');
  });
});

setShowAction(router);

module.exports = router;