var express = require('express');
var router = express.Router();

var clientsDAO = rootRequire('app/daos/clientsDAO');
var Client = rootRequire('app/model/client');

router.get('/client', function (req, res, next) {
  res.locals.client = req.currentClient;
  res.render('client/client/show');
});

router.get('/client/edit', function (req, res, next) {
  res.locals.client = req.currentClient;
  delete res.locals.client.password;
  res.render('client/client/edit');
});


router.patch('/client', function (req, res, next) {
  var client =  new Client(req.body);

  client.id = req.currentClient.id;
  client.name = req.currentClient.name;
  client.email = req.currentClient.email;

  clientsDAO.update(client, function(err) {
    if (err) {
      res.locals.client = client;
      res.render('client/client/edit');
      next(err);
    } else {
      res.redirect('/client-panel/client');
    }
  });

});

module.exports = router;
