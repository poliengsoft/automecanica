var express = require('express');
var router = express.Router();

var _ = require('underscore');

var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Vehicle = rootRequire('app/model/vehicle');

router.get('/vehicles', function (req, res, next) {
  vehiclesDAO.allByClientId(req.currentClient.id, function (err, vehicles) {
    if (err) next(err);
    else {
      res.locals.isClientNew = req.session.isClientNew;
      res.locals.vehicles = vehicles;
      res.render('client/vehicles/index');
    }
  });
});

router.get('/vehicles/new', function (req, res, next) {
  var vehicle = new Vehicle();
  res.locals.vehicle = vehicle;
  res.render('client/vehicles/new');
});

router.get('/vehicles/:id', function (req, res, next) {
  var clientId = req.currentClient.id;
  vehiclesDAO.allByClientId(clientId, function (err, vehicles) {
    if (err) return next(err);

    var vehicle = _.find(vehicles, function (vehicle) {
      return vehicle.id == req.params.id;
    });

    if (vehicle) {
      res.locals.vehicle = vehicle;
      res.render('client/vehicles/show');
    } else {
      res.redirect('/client-panel/vehicles');
    }
  });
});

router.post('/vehicles', function (req, res, next) {
  var vehicleForm = new Vehicle(req.body);
  vehicleForm.clientId = req.currentClient.id;
  vehiclesDAO.insert(vehicleForm, function (err, vehicle) {
    if (err) {
      req.flashCreateError('Veículo');
      res.locals.vehicle = vehicleForm;
      res.render('client/vehicles/new');
      next(err);
    }
    else {
      if (req.session.isClientNew) {
        res.redirect('/client-panel/schedules/new');
      } else {
        req.flashCreateSuccess('Veículo');
        res.locals.vehicle = vehicle;
        res.redirect('/client-panel/vehicles');
      }
    }
  });
});

router.get('/vehicles/:id/edit', function (req, res, next) {
  vehiclesDAO.selectById(req.params.id, function (err, vehicle) {
    if (err) next(err);
    res.locals.vehicle = vehicle;
    res.render('client/vehicles/edit');
  });
});

router.patch('/vehicles/:id', function (req, res, next) {
  var vehicle = new Vehicle(req.body);
  vehicle.clientId = req.currentClient.id;
  vehicle.id = req.params.id;
  vehiclesDAO.update(vehicle, function(err) {
    if (err) {
      req.flashUpdateError('Veículo');
      res.locals.vehicle = vehicle;
      res.render('client/vehicles/edit');
      next(err);
    } else {
      req.flashUpdateSuccess('Veículo');
      res.redirect('/client-panel/vehicles');
    }
  });
});

router.delete('/vehicles/:id', function (req, res, next) {
  var clientId = req.currentClient.id;
  vehiclesDAO.allByClientId(clientId, function (err, vehicles) {
    if (err) return next(err);

    var vehicle = _.find(vehicles, function (vehicle) {
      return vehicle.id == req.params.id;
    });

    if (vehicle) {
      vehiclesDAO.deleteById(vehicle.id, function (err) {
        res.redirect('/client-panel/vehicles');
      });
    } else {
      // Show error messages
      res.redirect('/client-panel/vehicles');
    }
  });
});

module.exports = router;
