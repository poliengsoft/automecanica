var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var clientsDAO = rootRequire('app/daos/clientsDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

module.exports = function (router) {
  router.get('/sos/:id',
    getSO,
    getSOVehicle,
    getCurrentClient,
    checkSOOwnership,
    getSOTeams,
    getSOServices,
    getSOAutoParts,
    renderSOShow
  );
};

function getSO(req, res, next) {
  sosDAO.selectById(req.params.id, function (err, so) {
    res.locals.so = so;
    next();
  });
}

function getSOVehicle(req, res, next) {
  var so = res.locals.so;
  vehiclesDAO.selectById(so.vehicleId, function (err, vehicle) {
    res.locals.vehicle = vehicle;
    next();
  });
}

function getCurrentClient(req, res, next) {
  res.locals.client = req.currentClient;
  next();
}

function checkSOOwnership(req, res, next) {
  var vehicle = res.locals.vehicle;
  var client = res.locals.client;
  if ( vehicle.clientId != client.id ) {
    res.redirect('client-panel/sos');
    return next(new Error('Client try to access other SO'));
  }

  next();
}

function getSOTeams(req, res, next) {
  var so = res.locals.so;
  teamsDAO.allBySOId(so.id, function (err, teams) {
    res.locals.teams = teams || [];
    next();
  });
}

function getSOServices(req, res, next) {
  var so = res.locals.so;
  servicesDAO.allBySOId(so.id, function (err, services) {
    res.locals.services = services;
    next();
  });
}

function getSOAutoParts(req, res, next) {
  var so = res.locals.so;
  autoPartsDAO.allBySOId(so.id, function (err, autoParts) {
    res.locals.autoParts = autoParts;
    next();
  });
}

function renderSOShow(req, res, next) {
  res.render('client/sos/show');
};