var express = require('express');
var router = express.Router();

var clientsDAO = rootRequire('app/daos/clientsDAO');

var passwordManager = rootRequire('app/services/passwordManager');

router.get('/password', function (req, res, next) {
  res.locals.client = {};
  res.render('client/client/password');
});

router.patch('/password', 
  validatePasswordConfirmation,
  validateCurrentPassword,
  updateCurrentPassword);

function validatePasswordConfirmation(req, res, next) {
  var form =  req.body;

  if (form.newPassword == form.newPasswordConfirmation
      && form.newPassword != '') return next();
  else {
    req.flash('error', 'A senha nova e a confirmações estão diferentes ou vazias');
    res.redirect('/client-panel/password');
  }
};

function validateCurrentPassword(req, res, next) {
  var form =  req.body;
  var currentClientPassword = req.currentClient.password;

  passwordManager.checkPassword(
    form.currentPassword, currentClientPassword,
    function (err, isPasswordValid) {
      if (err || !isPasswordValid){
        req.flash('error', 'Senha Atual incorreta');
        res.redirect('/client-panel/password');
      } 
      else return next();
    });
};

function updateCurrentPassword(req, res, next) {
  var client =  req.body;
  client.id = req.currentClient.id;

  passwordManager.hashPassword(client.newPassword, function (err, hash) {
    client.password = hash;
    clientsDAO.updatePassword(client, function(err) {
      if (err) {
        req.flashUpdateError('Houve uma falha para salvar');
        res.redirect('/client-panel/password');
        next(err);
      } else {
        req.flashUpdateSuccess('Senha');
        res.redirect('/client-panel/client');
      }
    });
  });
};

module.exports = router;
