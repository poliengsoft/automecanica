var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');
var Staff = rootRequire('app/model/staff');

router.get('/mechanic', function (req, res, next) {
  res.locals.staff = req.currentStaff;
  delete res.locals.staff.password;
  res.render('mechanic/mechanic/show');
});

router.get('/mechanic/edit', function (req, res, next) {
  res.locals.staff = req.currentStaff;
  delete res.locals.staff.password;
  res.render('mechanic/mechanic/edit');
});

router.patch('/mechanic', function (req, res, next) {
  var staff =  new Staff(req.body);

  staff.id = req.currentStaff.id;
  staff.name = req.currentStaff.name;
  staff.email = req.currentStaff.email;
  staff.role = req.currentStaff.role;

  staffsDAO.update(staff, function(err) {
    if (err) {
      res.locals.staff = staff;
      res.render('mechanic/mechanic/edit');
      next(err);
    } else {
      res.redirect('/mechanic-panel/mechanic');
    }
  });

});

module.exports = router;
