var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

module.exports = function (router) {
  router.get('/sos/:id/tasks',
    getSO,
    getSOVehicle,
    getSOServices,
    renderSOShow
  );
};

function getSO(req, res, next) {
  sosDAO.selectById(req.params.id, function (err, so) {
    res.locals.so = so;
    next();
  });
}

function getSOVehicle(req, res, next) {
  var so = res.locals.so;
  vehiclesDAO.selectById(so.vehicleId, function (err, vehicle) {
    res.locals.vehicle = vehicle;
    next();
  });
}

function getSOServices(req, res, next) {
  var so = res.locals.so;
  servicesDAO.allBySOId(so.id, function (err, services) {
    res.locals.services = services;
    next();
  });
}

function renderSOShow(req, res, next) {
  res.render('mechanic/sos/tasks');
};
