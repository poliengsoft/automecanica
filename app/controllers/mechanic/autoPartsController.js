var express = require('express');
var router = express.Router();

var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var AutoPart = rootRequire('app/model/autoPart');

router.get('/autoParts', function (req, res, next) {
  autoPartsDAO.all(function(err, autoParts) {
    res.locals.autoParts = autoParts;
    res.render('mechanic/autoParts/index');
  });
});

router.get('/autoParts/:id', function (req, res, next) {
  autoPartsDAO.selectById(req.params.id, function(err, autoPart) {
    if (err) {
      res.redirect('/mechanic-panel/autoParts');
      next(err);
    } else {
      res.locals.autoPart = autoPart;
      res.render('mechanic/autoParts/show');
    }
  });
});

module.exports = router;
