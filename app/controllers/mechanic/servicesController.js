var express = require('express');
var router = express.Router();

var servicesDAO = rootRequire('app/daos/servicesDAO');
var Service = rootRequire('app/model/service');

router.get('/services', function (req, res, next) {
  servicesDAO.all(function(err, services) {
    res.locals.services = services;
    res.render('mechanic/services/index');
  });
});

router.get('/services/:id', function (req, res, next) {
  servicesDAO.selectById(req.params.id, function(err, service) {
    if (err) {
      res.redirect('/mechanic-panel/services');
      next(err);
    } else {
      res.locals.service = service;
      res.render('mechanic/services/show');
    }
  });
});

module.exports = router;
