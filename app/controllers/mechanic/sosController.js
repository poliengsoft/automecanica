var express = require('express');
var router = express.Router();

var SO = rootRequire('app/model/so');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

var addShowAction = require('./sosController/show');
var addTasksAction = require('./sosController/tasks');

router.get('/sos', function (req, res, next) {
  sosDAO.allWithCarPlateFromMechanicId(req.currentStaff.id, function(err, sos){
    res.locals.sos = sos;
    res.render('mechanic/sos/index');
  })
});

addShowAction(router);
addTasksAction(router);

router.post('/sos/:id/tasks', function (req, res, next) {
  if (!req.body.servicesIds) {
    return res.redirect('/mechanic-panel/sos/'+req.params.id+'/tasks');
  }
  var so = {};
  so.id = req.params.id;
  so.tasksIds = req.body.servicesIds;
  if (!(so.tasksIds instanceof Array)) so.tasksIds = [so.tasksIds];
  servicesDAO.updateTasksFromSO(so, function (err, result) {
    if (err) {
      req.flashUpdateError('tarefas');
      res.redirect('/mechanic-panel/sos/'+req.params.id+'/tasks');
    }
    else {
      req.flashUpdateSuccess('tarefas');
      res.redirect('/mechanic-panel/sos/'+req.params.id+'/tasks');
    }
  });
});
module.exports = router;
