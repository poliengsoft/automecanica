var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');

var passwordManager = rootRequire('app/services/passwordManager');

router.get('/password', function (req, res, next) {
  res.locals.mechanic = {};
  res.render('mechanic/mechanic/password');
});

router.patch('/password', 
  validatePasswordConfirmation,
  validateCurrentPassword,
  updateCurrentPassword);

function validatePasswordConfirmation(req, res, next) {
  var form =  req.body;

  if (form.newPassword == form.newPasswordConfirmation 
      && form.newPassword != '') return next();
  else {
    req.flash('error', 'A senha nova e a confirmações estão diferentes ou vazias');
    res.redirect('/mechanic-panel/password');
  }
};

function validateCurrentPassword(req, res, next) {
  var form =  req.body;
  var currentStaffPassword = req.currentStaff.password;

  passwordManager.checkPassword(
    form.currentPassword, currentStaffPassword,
    function (err, isPasswordValid) {
      if (err || !isPasswordValid) {
        req.flash('error', 'Senha Atual incorreta');
        res.redirect('/mechanic-panel/password');
      }
      else return next();
    });
};

function updateCurrentPassword(req, res, next) {
  var mechanic =  req.body;
  mechanic.id = req.currentStaff.id;

  passwordManager.hashPassword(mechanic.newPassword, function (err, hash) {
    mechanic.password = hash;
    staffsDAO.updatePassword(mechanic, function(err) {
      if (err) {
        req.flashUpdateError('Houve uma falha para salvar');
        res.redirect('/mechanic-panel/password');
        next(err);
      } else {
        req.flashUpdateSuccess('Senha');
        res.redirect('/mechanic-panel/mechanic');
      }
    });
  });
};

module.exports = router;
