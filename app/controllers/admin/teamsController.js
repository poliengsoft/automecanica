var express = require('express');
var router = express.Router();

var teamsDAO = rootRequire('app/daos/teamsDAO');
var Team = rootRequire('app/model/team');

router.get('/teams', function (req, res, next) {
  teamsDAO.all(function(err, teams) {
    res.locals.teams = teams;
    res.render('admin/teams/index');
  });
});

router.get('/teams/new', function (req, res, next) {
  teamsDAO.allMechanics(function(err, mechanics) {
    res.locals.team = new Team();
    res.locals.mechanics = mechanics;
    res.render('admin/teams/new');
  });
});

router.get('/teams/:id', function (req, res, next) {
  teamsDAO.selectById(req.params.id, function(err, team) {
    if (err) {
      req.flashNotFound('Equipe');
      res.redirect('/admin-panel/teams');
      next(err);
    } else {
      res.locals.team = team;
      res.render('admin/teams/show');
    }
  });
});

router.post('/teams', function (req, res, next) {
  var teamForm = new Team(req.body);
  teamsDAO.insertTeamWithMechanics(teamForm, function(err, team) {
    if (err) {
      res.locals.team = teamForm;
      teamsDAO.allMechanics(function(err, mechanics) {
        res.locals.mechanics = mechanics;
        req.flashCreateError('Equipe');
        res.render('admin/teams/new');
      });
    }
    else {
      req.flashCreateSuccess('Equipe');
      res.redirect('/admin-panel/teams');
    }
  });
});

router.get('/teams/:id/edit', function (req, res, next) {
  teamsDAO.selectById(req.params.id, function(err, team) {
    if (err) {
      req.flashNotFound('Equipe');
      res.redirect('/admin-panel/teams');
      next(err);
    } else {
      res.locals.team = team;
      teamsDAO.allMechanics(function(err, mechanics) {
        res.locals.mechanics = mechanics;
        res.render('admin/teams/edit');
      });
    }
  });
});

router.patch('/teams/:id', function (req, res, next) {
  var team =  new Team(req.body);
  team.id = req.params.id;
  teamsDAO.updateTeamWithMechanics(team, function (err) {
    if (err) {
      res.locals.team = team;
      teamsDAO.allMechanics(function(err, mechanics) {
        res.locals.mechanics = mechanics;
        req.flashUpdateError('Equipe');
        res.render('admin/teams/edit');
      });
    }
    else {
      req.flashUpdateSuccess('Equipe');
      res.redirect('/admin-panel/teams');
    }
  });
});

router.delete('/teams/:id', function (req, res, next) {
  teamsDAO.deleteById(req.params.id, function(err) {
    if (err) {
      req.flashDeleteError('Equipe');
    } else {
      req.flashDeleteSuccess('Equipe');
    }
    res.redirect('/admin-panel/teams');
  });
});

module.exports = router;
