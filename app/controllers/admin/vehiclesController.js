var express = require('express');
var router = express.Router();

var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var Vehicle = rootRequire('app/model/vehicle');

router.get('/vehicles', function (req, res, next) {
  vehiclesDAO.all(function(err, vehicles) {
    res.locals.vehicles = vehicles;
    res.render('admin/vehicles/index');
  });
});

router.get('/vehicles/:id', function (req, res, next) {
  vehiclesDAO.selectById(req.params.id, function(err, vehicle) {
    if (err) {
      req.flashNotFound('Veículo');
      res.redirect('/admin-panel/clients');
    } else {
      res.locals.vehicle = vehicle;
      res.render('admin/vehicles/show');
    }
  });
});

module.exports = router;
