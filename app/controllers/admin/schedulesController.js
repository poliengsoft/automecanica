var express = require('express');
var router = express.Router();

var schedulesDAO = rootRequire('app/daos/schedulesDAO');
var clientsDAO = rootRequire('app/daos/clientsDAO');

router.get('/schedules', function (req, res, next) {
  schedulesDAO.allWithClientName(function(err, schedules) {
    res.locals.schedules = schedules;
    res.render('admin/schedules/index');
  });
});

router.get('/schedules/:id', function (req, res, next) {
  schedulesDAO.selectWithClientNameById(req.params.id, function(err, schedule) {
    if (err) {
      res.redirect('/admin-panel/schedules');
      next(err);
    } else {
      res.locals.schedule = schedule;
      res.render('admin/schedules/show');
    }
  });
});

module.exports = router;