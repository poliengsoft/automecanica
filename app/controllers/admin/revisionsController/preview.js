var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');

module.exports = function (router) {
  router.post('/revisions/preview',
    preparePreview,
    getServices,
    getAutoParts,
    renderRevisionPreview
  );
};

function preparePreview(req, res, next) {
  res.locals.name = req.body.name;
  res.locals.description = req.body.description;
  next();
};

function getServices(req, res, next) {
  var servicesIds = req.body['servicesIds[]'];
  servicesDAO.selectAllByIds(servicesIds, function(err, services) {
    if (err) return next(err);
    res.locals.services = services;
    next();
  });
};

function getAutoParts(req, res, next) {
  var autoPartsIds = req.body['autoPartsIds[]'];
  autoPartsDAO.selectAllByIds(autoPartsIds, function(err, autoParts) {
    if (err) return next(err);
    res.locals.autoParts = autoParts;
    next();
  });
};

function renderRevisionPreview(req, res, next) {
  res.render('admin/revisions/preview');
};