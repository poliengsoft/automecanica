var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var Revision = rootRequire('app/model/revision');

module.exports = function (router) {
  router.get('/revisions/new',
    getServices,
    getAutoParts,
    renderRevision
  );
};

function getAutoParts(req, res, next) {
  autoPartsDAO.all(function(err, autoParts) {
    if (err) {
      req.flashNotFound('peças'); 
      return next(err);
    }
    res.locals.autoParts = autoParts;
    next();
  });
};

function getServices(req, res, next) {
  servicesDAO.all(function(err, services) {
    if (err) {
      req.flashNotFound('serviços'); 
      return next(err);
    }
    res.locals.services = services;
    next();
  });
};

function renderRevision(req, res, next) {
  res.locals.revision = new Revision();
  res.render('admin/revisions/new');
};