var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var Revision = rootRequire('app/model/revision');

module.exports = function (router) {
  router.post('/revisions',
    prepareRevision,
    getRevisionServices,
    getRevisionAutoParts,
    createRevision
  );
};

function prepareRevision(req, res, next) {
  revision = new Revision(req.body);
  res.locals.revision = revision;
  next();
}

function getRevisionServices(req, res, next) {
  var servicesIds = req.body.servicesIds;
  servicesIds = convertToArray(servicesIds);
  servicesDAO.selectByListOfIds(servicesIds, function (err, result) {
    if (err) {
      return next(err);
    }
      res.locals.revision.services = result; // Here services changes to tasks
    next();
  });
};

function getRevisionAutoParts(req, res, next) {
  var autoPartsIds = req.body.autoPartsIds;
  autoPartsIds = convertToArray(autoPartsIds);
  autoPartsDAO.selectByListOfIds(autoPartsIds, function (err, result) {
    if (err) {
      return next(err);
    }
    res.locals.revision.autoParts = result;
    next();
  });
};

function createRevision(req, res, next) {
  revisionsDAO.insert(res.locals.revision, function (err, result) {
    if (err) {
      delete res.locals.revision.id;
      req.flashCreateError('Revisão');
      res.redirect('/admin-panel/revisions/new');
      return next(err);
    }
    else {
      req.flashCreateSuccess('Revisão');
      res.redirect('/admin-panel/revisions');
    }
  });
};

function convertToArray(obj) {
  return ( obj instanceof Array) ? obj : [obj];
};
