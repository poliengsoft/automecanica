var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var revisionsDAO = rootRequire('app/daos/revisionsDAO');

module.exports = function (router) {
  router.get('/revisions/:id',
    getRevision,
    getRevisionServices,
    getRevisionAutoParts,
    renderRevisionShow
  );
};

function getRevision(req, res, next) {
  revisionsDAO.selectById(req.params.id, function (err, revision) {
    res.locals.revision = revision;
    next();
  });
}

function getRevisionServices(req, res, next) {
  var revision = res.locals.revision;
  servicesDAO.allByRevisionId(revision.id, function (err, services) {
    res.locals.services = services;
    next();
  });
}

function getRevisionAutoParts(req, res, next) {
  var revision = res.locals.revision;
  autoPartsDAO.allByRevisionId(revision.id, function (err, autoParts) {
    res.locals.autoParts = autoParts;
    next();
  });
}

function renderRevisionShow(req, res, next) {
  res.render('admin/revisions/show');
};
