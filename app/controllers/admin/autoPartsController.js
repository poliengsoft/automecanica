var express = require('express');
var router = express.Router();

var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var AutoPart = rootRequire('app/model/autoPart');

router.get('/autoParts', function (req, res, next) {
  autoPartsDAO.all(function(err, autoParts) {
    res.locals.autoParts = autoParts;
    res.render('admin/autoParts/index');
  });
});

router.get('/autoParts/new', function (req, res, next) {
  res.locals.autoPart = new AutoPart();
  res.render('admin/autoParts/new');
});

router.get('/autoParts/:id', function (req, res, next) {
  autoPartsDAO.selectById(req.params.id, function(err, autoPart) {
    if (err) {
      req.flashNotFound('Peças');
      res.redirect('/admin-panel/autoParts');
      next(err);
    } else {
      res.locals.autoPart = autoPart;
      res.render('admin/autoParts/show');
    }
  });
});

router.post('/autoParts', function (req, res, next) {
  var autoPartForm = new AutoPart(req.body);

  autoPartsDAO.insert(autoPartForm, function(err, autoPart) {
    if (err) {
      res.locals.autoPart = autoPartForm;
      req.flashCreateError('Peças');
      res.render('admin/autoParts/new');
      next(err);
    } else {
      req.flashCreateSuccess('Peças');
      res.redirect('/admin-panel/autoParts');
    }
  });
});

router.get('/autoParts/:id/edit', function (req, res, next) {
  autoPartsDAO.selectById(req.params.id, function(err, autoPart) {
    if (err) {
      req.flashNotFound('Peças');
      res.redirect('/admin-panel/autoParts');
      next(err);
    } else {
      res.locals.autoPart = autoPart;
      res.render('admin/autoParts/edit');
    }
  });
});

router.patch('/autoParts/:id', function (req, res, next) {
  var autoPart =  new AutoPart(req.body);
  autoPart.id = req.params.id;

  autoPartsDAO.update(autoPart, function(err) {
    if (err) {
      res.locals.autoPart = autoPart;
      req.flashUpdateError('Peças');
      res.render('admin/autoParts/edit');
      next(err);
    } else {
      res.locals.autoPart = autoPart;
      req.flashUpdateSuccess('Peças');
      res.redirect('/admin-panel/autoParts');
    }
  });
});

router.delete('/autoParts/:id', function (req, res, next) {
  autoPartsDAO.deleteById(req.params.id, function(err) {
    if (err) {
      req.flashDeleteError('Peças');
    } else {
      req.flashDeleteSuccess('Peças');
    }
    res.redirect('/admin-panel/autoParts');
  });
});

module.exports = router;
