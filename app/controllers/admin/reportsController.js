var express = require('express');
var router = express.Router();

var schedulesDAO = rootRequire('app/daos/schedulesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

var moment = require('moment');

router.get('/reports/search',
  parseDates,
  countAllSchedules,
  getAllSOS,
  filterCancelledSOS,
  filterFinishedSOS,
  filterPayedSOS,
  calculateIncome,
  renderReports
);

function parseDates(req, res, next) {
  var startDate = req.query.startDate;
  startDate = startDate ? moment(startDate) : moment().subtract(7, 'days');
  
  var endDate = req.query.endDate;
  endDate = endDate ? moment(endDate) : moment();

  if ( endDate.isBefore(startDate) ) {
    var tempDate = startDate;
    startDate = endDate;
    endDate = tempDate;
  };

  res.locals.report = {
    startDate: startDate,
    endDate: endDate
  };

  next();
}

function countAllSchedules(req, res, next) {
  var startDate = res.locals.report.startDate.toDate();
  var endDate = res.locals.report.endDate.toDate();

  schedulesDAO.countAllBetween(startDate, endDate, function(err, schedulesTotal) {
    res.locals.schedulesTotal = schedulesTotal;
    next();
  });
}

function getAllSOS(req, res, next) {
  var startDate = res.locals.report.startDate.toDate();
  var endDate = res.locals.report.endDate.toDate();

  sosDAO.allBetween(startDate, endDate, function(err, sos) {
    res.locals.sos = sos;
    next();
  });
}

function filterCancelledSOS(req, res, next) {
  res.locals.cancelledSOS = res.locals.sos.filter(function(so) {
    return so.cancellationReason !== null;
  });
  next();
}

function filterFinishedSOS(req, res, next) {
  res.locals.finishedSOS = res.locals.sos.filter(function(so) {
    return so.isFinished;
  });
  next();
}

function filterPayedSOS(req, res, next) {
  res.locals.payedSOS = res.locals.sos.filter(function(so) {
    return so.paymentStatus === 'Confirmado';
  });
  next();
}

function calculateIncome(req, res, next) {
  res.locals.income = res.locals.payedSOS.reduce(function(sum, so) {
    return sum + so.price;
  }, 0);
  next();
}

function renderReports(req, res, next) {
  res.render('admin/reports/search');
}

module.exports = router;