var express = require('express');
var router = express.Router();

var servicesDAO = rootRequire('app/daos/servicesDAO');
var Service = rootRequire('app/model/service');

router.get('/services', function (req, res, next) {
  servicesDAO.all(function(err, services) {
    res.locals.services = services;
    res.render('admin/services/index');
  });
});

router.get('/services/new', function (req, res, next) {
  res.locals.service = new Service();
  res.render('admin/services/new');
});

router.get('/services/:id', function (req, res, next) {
  servicesDAO.selectById(req.params.id, function(err, service) {
    if (err) {
      req.flashNotFound('Serviço');
      res.redirect('/admin-panel/services');
      next(err);
    } else {
      res.locals.service = service;
      res.render('admin/services/show');
    }
  });
});

router.post('/services', function (req, res, next) {
  var serviceForm = new Service(req.body);

  servicesDAO.insert(serviceForm, function(err, service) {
    if (err) {
      res.locals.service = serviceForm;
      req.flashCreateError('Serviço');
      res.render('admin/services/new');
      next(err);
    } else {
      req.flashCreateSuccess('Serviço');
      res.redirect('/admin-panel/services');
    }
  });
});

router.get('/services/:id/edit', function (req, res, next) {
  servicesDAO.selectById(req.params.id, function(err, service) {
    if (err) {
      req.flashNotFound('Serviço');
      res.redirect('/admin-panel/services');
      next(err);
    } else {
      res.locals.service = service;
      res.render('admin/services/edit');
    }
  });
});

router.patch('/services/:id', function (req, res, next) {
  var service =  new Service(req.body);
  service.id = req.params.id;

  servicesDAO.update(service, function(err) {
    if (err) {
      res.locals.service = service;
      req.flashUpdateError('Serviço');
      res.render('admin/services/edit');
      next(err);
    } else {
      req.flashUpdateSuccess('Serviço');
      res.redirect('/admin-panel/services');
    }
  });
});

router.delete('/services/:id', function (req, res, next) {
  servicesDAO.deleteById(req.params.id, function(err) {
    if (err) {
      req.flashDeleteError('Serviço');
    } else {
      req.flashDeleteSuccess('Serviço');
    }
    res.redirect('/admin-panel/services');
  });
});

module.exports = router;
