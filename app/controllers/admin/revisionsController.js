var express = require('express');
var router = express.Router();

var revisionsDAO = rootRequire('app/daos/revisionsDAO');
var Revision = rootRequire('app/model/revision');

var setNewAction = require('./revisionsController/new');
var setPreviewAction = require('./revisionsController/preview');
var setShowAction = require('./revisionsController/show');
var setCreateAction = require('./revisionsController/create');

router.get('/revisions', function (req, res, next) {
  revisionsDAO.all(function(err, revisions) {
    res.locals.revisions = revisions;
    res.render('admin/revisions/index');
  });
});

setNewAction(router);
setPreviewAction(router);
setShowAction(router);
setCreateAction(router);

module.exports = router;
