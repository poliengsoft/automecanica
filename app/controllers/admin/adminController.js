var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');
var Staff = rootRequire('app/model/staff');

router.get('/admin', function (req, res, next) {
  res.locals.staff = req.currentStaff;
  delete res.locals.staff.password;
  res.render('admin/admin/show');
});

router.get('/admin/edit', function (req, res, next) {
  res.locals.staff = req.currentStaff;
  delete res.locals.staff.password;
  res.render('admin/admin/edit');
});


router.patch('/admin', function (req, res, next) {
  var staff =  new Staff(req.body);

  staff.id = req.currentStaff.id;
  staff.name = req.currentStaff.name;
  staff.email = req.currentStaff.email;
  staff.role = req.currentStaff.role;

  staffsDAO.update(staff, function(err) {
    if (err) {
      res.locals.staff = staff;
      res.render('admin/admin/edit');
      next(err);
    } else {
      res.redirect('/admin-panel/admin');
    }
  });

});

module.exports = router;
