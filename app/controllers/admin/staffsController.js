var express = require('express');
var router = express.Router();

var staffsDAO = rootRequire('app/daos/staffsDAO');
var Staff = rootRequire('app/model/staff');
var mailer = rootRequire('app/services/mailerService');

var passwordManager = rootRequire('app/services/passwordManager');

router.get('/staffs', function (req, res, next) {
  staffsDAO.all(function(err, staffs) {
    res.locals.staffs = staffs;
    res.render('admin/staffs/index');
  });
});

router.get('/staffs/new', function (req, res, next) {
  res.locals.staff = new Staff();
  res.locals.PERMITTED_ROLES = Staff.PERMITTED_ROLES;
  res.render('admin/staffs/new');
});

router.get('/staffs/:id', function (req, res, next) {
  staffsDAO.selectById(req.params.id, function(err, staff) {
    if (err) {
      req.flashNotFound('Funcionário');
      res.redirect('/admin-panel/staffs');
      next(err);
    } else {
      res.locals.staff = staff;
      res.render('admin/staffs/show');
    }
  });
});

router.post('/staffs', function (req, res, next) {
  var staffForm = new Staff(req.body);

  passwordManager.hashPassword(staffForm.password, function (err, hash) {
    staffForm.password = hash;
    staffsDAO.insert(staffForm, function(err, staff) {
      if (err) {
        delete staffForm.password;
        res.locals.staff = staffForm;
        req.flashCreateError('Funcionário');
        res.locals.PERMITTED_ROLES = Staff.PERMITTED_ROLES;
        res.render('admin/staffs/new');
        next(err);
      } else {
        req.flashCreateSuccess('Funcionário');
        res.redirect('/admin-panel/staffs');
        // mailer.sendWelcomeToStaff(staff);
      }
    });
  });
});

router.get('/staffs/:id/edit', function (req, res, next) {
  staffsDAO.selectById(req.params.id, function(err, staff) {
    if (err) {
      req.flashNotFound('Funcionário');
      res.redirect('/admin-panel/staffs');
      next(err);
    } else {
      res.locals.PERMITTED_ROLES = Staff.PERMITTED_ROLES;
      res.locals.staff = staff;
      res.render('admin/staffs/edit');
    }
  });
});

router.patch('/staffs/:id', function (req, res, next) {
  var staff =  new Staff(req.body);
  staff.id = req.params.id;

  staffsDAO.update(staff, function(err) {
    if (err) {
      res.locals.staff = staff;
      res.locals.PERMITTED_ROLES = Staff.PERMITTED_ROLES;
      req.flashUpdateError('Funcionário');
      res.render('admin/staffs/edit');
      next(err);
    } else {
      req.flashUpdateSuccess('Funcionário');
      res.redirect('/admin-panel/staffs');
    }
  });
});

router.delete('/staffs/:id', function (req, res, next) {
  staffsDAO.deleteById(req.params.id, function(err) {
    if (err) {
      req.flashDeleteError('Funcionário');
    } else {
      req.flashDeleteSuccess('Funcionário');
    }
    res.redirect('/admin-panel/staffs');
  });
});

module.exports = router;
