var express = require('express');
var router = express.Router();

var SO = rootRequire('app/model/so');
var vehiclesDAO = rootRequire('app/daos/vehiclesDAO');
var teamsDAO = rootRequire('app/daos/teamsDAO');
var autoPartsDAO = rootRequire('app/daos/autoPartsDAO');
var servicesDAO = rootRequire('app/daos/servicesDAO');
var sosDAO = rootRequire('app/daos/sosDAO');

var setShowAction = require('./sosController/show');

router.get('/sos', function (req, res, next) {
  sosDAO.allWithCarPlate(function(err, sos){
    res.locals.sos = sos;
    res.render('admin/sos/index');
  })
});

setShowAction(router);

module.exports = router;
