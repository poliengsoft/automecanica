var BaseModel = require('./__base__');

module.exports = (function() {
  SO.prototype.__proto__ = BaseModel.prototype;

  function SO() {
    BaseModel.apply(this, arguments);
  };

  SO.prototype.collectionName = 'sos';

  return SO
})();
