var BaseModel = require('./__base__');

module.exports = (function() {
  Client.prototype.__proto__ = BaseModel.prototype;

  function Client() {
    BaseModel.apply(this, arguments);
  };

  Client.prototype.collectionName = 'clients';
  Client.prototype.isClient = true;
  Client.prototype.type = 'Client';

  return Client
})();
