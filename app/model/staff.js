var BaseModel = require('./__base__');

module.exports = (function() {
  Staff.prototype.__proto__ = BaseModel.prototype;

  function Staff() {
    BaseModel.apply(this, arguments);
  };

  Staff.prototype.collectionName = 'staffs';
  Staff.prototype.isStaff = true;
  Staff.prototype.type = 'Staff';
  Staff.PERMITTED_ROLES = [
    'Gerente',
    'Atendente',
    'Técnico',
    'Mecânico'
  ]

  return Staff
})();
