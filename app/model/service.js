var BaseModel = require('./__base__');

module.exports = (function() {
  Service.prototype.__proto__ = BaseModel.prototype;

  function Service() {
    BaseModel.apply(this, arguments);
  };

  Service.prototype.collectionName = 'services';

  return Service
})();
