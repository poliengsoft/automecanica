module.exports = (function() {

  function BaseModel(baseObject) {
    if (baseObject){
      this.inheritFrom(baseObject);
    } 
  };

  BaseModel.prototype.isPersisted = function() {
    return !!this.id;
  };

  BaseModel.prototype.inheritFrom = function(baseObject) {
    for (var key in baseObject) {
      this[key] = baseObject[key];
    }
  };

  return BaseModel
})();