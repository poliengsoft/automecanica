var BaseModel = require('./__base__');

module.exports = (function() {
  Vehicle.prototype.__proto__ = BaseModel.prototype;

  function Vehicle() {
    BaseModel.apply(this, arguments);
  };

  Vehicle.prototype.collectionName = 'vehicles';

  return Vehicle
})();
