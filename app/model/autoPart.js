var BaseModel = require('./__base__');

module.exports = (function() {
  Part.prototype.__proto__ = BaseModel.prototype;

  function Part() {
    BaseModel.apply(this, arguments);
  };

  Part.prototype.collectionName = 'autoParts';

  return Part
})();
