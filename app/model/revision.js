var BaseModel = require('./__base__');

module.exports = (function() {
  Revision.prototype.__proto__ = BaseModel.prototype;

  function Revision() {
    BaseModel.apply(this, arguments);
  };

  Revision.prototype.collectionName = 'revisions';

  return Revision
})();
