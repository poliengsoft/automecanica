var BaseModel = require('./__base__');

module.exports = (function() {
  Team.prototype.__proto__ = BaseModel.prototype;

  function Team() {
    BaseModel.apply(this, arguments);
  };

  Team.prototype.collectionName = 'teams';

  return Team
})();
